/**
 * @file rest-test.js
 * 
 * API testing of sensemapserver
 * 
 * @author kocan
 */

var should = require('should');
var assert = require('assert');
var request = require('supertest');
const addContext = require('mochawesome/addContext');

var configIni = require('config.ini');
var conf = configIni.load('./config.ini');  

var IP = conf.SectionOne.IP_ADDRESS;

(conf.SectionTwo.REST_TEST ? describe : describe.skip)('REST Tests', function() {
    var url = 'http://'+IP+'/sensmapserver';
    var feeds;
    var anchorsCount;
    var tagsCount;
    var buildingsCount;
    var newAnchorID;
    var newTagID;
    var buildingID;
    var buildingIDs = [];
    var newFeedID;
    var newZoneID;
    var newPathID;
    var newModelID;

    before(function(done) {
        //TODO REMOVE ALL TEMPORARY BUILDINGS/TAGS/ANCHORS from previous (unsuccessful runs) that stayed in DB
        done();
    });

    function testJSON(text) {
        try {
            var json = JSON.parse(text);
            return true;
        } catch (error) {
            return false;
        }
    }


    function countFeedType (feeds, type) {
        var count = 0;
        for (var f = 0; f < feeds.length; f++) {
            if (feeds[f].type === type) {
                count++;
            }
        }
        return count;
    } 

    //TODO test pagination
    describe('Feeds', function() {
        it('should have context', function (done) {
            (1+1).should.equal(2);
            addContext(this, 'context');
            done();
          });

        it('should get list of all feeds (no feeds for tags, anchors and buildings) in JSON with response code 200', function(done) {
            request(url)
                .get('/api/feeds')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
            // end handles the response
            .end(function(err, res) {
                if (err) {
                    throw err;
                }
                res.status.should.be.equal(200);
                var isJson = testJSON(res.text);
                isJson.should.be.true();
                feeds = JSON.parse(res.text);
                anchorsCount = countFeedType(feeds.results, "anchor");
                tagsCount = countFeedType(feeds.results, "tag");
                buildingsCount = countFeedType(feeds.results, "building");

                tagsCount.should.be.equal(0);
                anchorsCount.should.be.equal(0);
                buildingsCount.should.be.equal(0);
                res.should.be.html;

                done();
            });
        });

        it('should create new feed', function(done) {
            var feed = {
                title: 'this_will_be_my_new_building' + Math.floor(Math.random() * 0xffffffffff).toString(16), //generate random name of feed
                tags:["one_feed_metatag"],
                datastreams: [{"id":"clr","current_value":"0","at":"2016-06-11 18:24:07.597976","unit":{"symbol":"","label":""},"datapoints":[]}]
            };

            request(url)
                .post('/api/feeds')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(feed)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonAnchor = JSON.parse(res.text);
                    res.status.should.be.equal(200);
                    (jsonAnchor).should.have.ownProperty('feed_id');
                    newFeedID = jsonAnchor.feed_id;
                    newFeedID.should.be.above(0);
                    done();
                });
        });

        it('should get newly created feed', function(done) {
            request(url)
                .get('/api/feeds/' + newFeedID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    done();
                });
        });    

        it('should edit newly created feed', function(done) {
            var feed_edit = {
                "description": 'edit_test'           
            };            
            request(url)
                .put('/api/feeds/' + newFeedID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(feed_edit)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    done();
                });
        });      
        
        it('should get newly edited feed', function(done) {           
            request(url)
                .get('/api/feeds/' + newFeedID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonAnchor = JSON.parse(res.text);
                    res.status.should.be.equal(200);
                    res.body.description.should.be.equal('edit_test');
                    done();
                });
        });     
        
        // it('should get newly edited datastream', function(done) {           
        //     request(url)
        //         .get('/api/feeds/' + newFeedID + '/datastreams/clr')
        //         .set({
        //             'X-APIKey': '17254faec6a60f58458308763'
        //         })
        //         // end handles the response
        //         .end(function(err, res) {
        //             if (err) {
        //                 throw err;
        //             }
        //             console.log(newFeedID);
        //             res.status.should.be.equal(200);
        //             done();
        //         });
        // });           

        it('shouldnt delete newly created feed because wrong API', function(done) {
            request(url)
                .delete('/api/feeds/' + newFeedID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308764'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(401);
                    done();
                });
        });  

        it('should delete newly created feed', function(done) {
            request(url)
                .delete('/api/feeds/' + newFeedID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    done();
                });
        });        

        it('should create new feed', function(done) {
            var feed = {
                title: 'this_will_be_my_new_building' + Math.floor(Math.random() * 0xffffffffff).toString(16), //generate random name of feed
                tags:["one_feed_metatag"]
            };

            request(url)
                .post('/api/feeds')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(feed)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonAnchor = JSON.parse(res.text);
                    res.status.should.be.equal(200);
                    (jsonAnchor).should.have.ownProperty('feed_id');
                    newFeedID = jsonAnchor.feed_id;
                    newFeedID.should.be.above(0);
                    done();
                });


        });        

    });
    describe('Buildings', function() {
        it('should transform latest feed to building', function(done) {
            request(url)
                .put('/api/buildings/' + newFeedID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send({
                    tags: ["second_feed_metatag"]
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });
        it('should get newly created transformed building in JSON with response code 200', function(done) {
            request(url)
                .get('/api/buildings/' + newFeedID)
                // end handles the response
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jstags = JSON.parse(res.text);
                    jstags.tags.length.should.be.equal(2); //it should keep both tags: one_feed_metatag, second_feed_metatag
                    jstags.title.indexOf("Building:").should.be.equal(0);
                    jstags.type.should.be.equal("building");
                    done();
                });
        });
        it('should not get newly created tag in /anchors', function(done) {
            request(url)
                .get('/api/anchors/' + newFeedID)
                // end handles the response
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(404)
                    done();
                });
        });
        it('should delete recently updated building (api key present in headers)', function(done) {
            request(url)
                .delete('/api/buildings/' + newFeedID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });

        it('should get list of all buildings in JSON with response code 200', function(done) {
            request(url)
                .get('/api/buildings')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jsbuildings = JSON.parse(res.text);
                    jsbuildings.results.length.should.be.equal(buildingsCount);
                    done();
                });
        });

        it('should create new building without title with response 400', function(done) {
            var tag = {
                tags: ['just_some_tag_to_identify_new_buildings', 'and_another_tag'],

                "location": {
                    "disposition": "fixed",
                    "ele": "j",
                    "name": "Building:b",
                    "lat": "0",
                    "exposure": "indoor",
                    "lon": "0",
                    "domain": ""
                }
            };

            request(url)
                .post('/api/buildings')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(tag)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(400);
                    done();
                });


        });
        it('should create new building without prefix "Building:"', function(done) {

            var bname = 'building_' + Math.floor(Math.random() * 0xffffffffff).toString(16);
            var tag = {
                title: bname, //generate random name of building
                tags: ['just_some_tag_to_identify_new_buildings', 'and_another_tag'],

                location: {
                    "disposition": "fixed",
                    "lat": "10", //should be kept at this value
                    "exposure": "indoor", //this should be fixed by server to outdoor
                    "lon": "20",
                    "domain": "" //should be set to physical
                }
            };

            request(url)
                .post('/api/buildings')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(tag)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonb = JSON.parse(res.text);
                    res.status.should.be.equal(200);
                    (jsonb).should.have.ownProperty('feed_id');
                    buildingIDs.push(jsonb.feed_id);
                    jsonb.feed_id.should.be.above(0);
                    jsonb.title.should.be.equal("Building:" + bname);
                    jsonb.location.lat.should.be.equal(10);
                    jsonb.location.lon.should.be.equal(20);
                    jsonb.location.domain.should.be.equal("physical");
                    jsonb.location.exposure.should.be.equal("outdoor");
                    done();
                });


        });
        it('should create new building with prefix "Building:"', function(done) {

            var bname = 'Building:building_' + Math.floor(Math.random() * 0xffffffff).toString(16);
            var tag = {
                title: bname, //generate random name of building
                tags: ['just_some_tag_to_identify_new_buildings', 'and_another_metatag'],

                location: {
                    "disposition": "fixed",
                    "exposure": "indoor", //this should be fixed by server to outdoor
                    "domain": "" //should be set to physical
                }
            };

            request(url)
                .post('/api/buildings')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(tag)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonb = JSON.parse(res.text);
                    res.status.should.be.equal(200);
                    (jsonb).should.have.ownProperty('feed_id');
                    buildingIDs.push(jsonb.feed_id);
                    jsonb.feed_id.should.be.above(0);
                    jsonb.title.should.be.equal(bname);
                    jsonb.location.lat.should.be.equal(0);
                    jsonb.location.lon.should.be.equal(0);
                    jsonb.location.domain.should.be.equal("physical");
                    jsonb.location.exposure.should.be.equal("outdoor");
                    done();
                });


        });
        it('should create new plan for latest building', function(done) {
            var plan = {
                "name": "plan_name2", //generate random name of building
                "url": "url planu",
                "scale": "2",
                "originX":"0",
                "originY":"11.88"
            };
            request(url)
                .post('/api/buildings/'+buildingIDs[1]+'/plans')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(plan)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    //jsonAnchor.should.have.ownProperty('feed_id');

                    done();
                });
        });
        it('should create second new plan for latest building', function(done) {
            var plan = {
                "name": "plan_name", //generate random name of building
                "url": "url plan",
                "scale": "2",
                "originX":"0",
                "originY":"11.88"
            };
            request(url)
                .post('/api/buildings/'+buildingIDs[1]+'/plans')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(plan)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);

                    done();
                });
        });
        it('should not allow to create new plan for latest building with the same name as previous plan', function(done) {
            var plan = {
                "name": "plan_name", //generate random name of building
                "url": "url plan",
                "scale": "2",
                "originX":"0",
                "originY":"11.88"
            };
            request(url)
                .post('/api/buildings/'+buildingIDs[1]+'/plans')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(plan)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(409);

                    done();
                });
        });
        it('should get all plans from latest building', function(done) {
            request(url)
                .get('/api/buildings/'+buildingIDs[1]+'/plans')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var plans = JSON.parse(res.text);

                    Object.keys(plans).length.should.be.equal(2);
                    var found = false;
                    for (var i in plans){
                        if (plans[i].name == "plan_name"){
                            found = true;
                            plans[i].name.should.equal("plan_name");
                            plans[i].url.should.equal("url plan");
                            plans[i].scale.should.equal("2");
                            plans[i].originX.should.equal("0");
                            plans[i].originY.should.equal("11.88");
                        }
                    }
                    found.should.be.true();
                    done();
                });
        });

        it('should update plan plan_name to new name new_plan_name and set new url, scale and axis', function(done) {
            var plan = {
                "name": "new_plan_name", //generate random name of building
                "url": "url planu2",
                "scale": "2.99999",
                "originX":"0.0000008",
                "originY":"11.888888"
            };
            request(url)
                .put('/api/buildings/'+buildingIDs[1]+'/plans/'+"plan_name")
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(plan)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);

                    done();
                });
        });

        it('should not allow to update plan with name plan_name because we just renamed it', function(done) {
            var plan = {
                "name": "new_new_plan_name", //generate random name of building
                "url": "url planu",
                "scale": "2",
                "originX":"0",
                "originY":"11.88"
            };
            request(url)
                .put('/api/buildings/'+buildingIDs[1]+'/plans/plan_name')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(plan)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    res.status.should.be.equal(404)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    done();
                });
        });
        it('should not allow to add new plan because we have not defined name, scale, origin and url', function(done) {
            var plan = {
               "name":"just some name",
               "origiX":"bla"
            };
            request(url)
                .post('/api/buildings/'+buildingIDs[1]+'/plans')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(plan)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    res.status.should.be.equal(400)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    done();
                });
        });
        it('should get all plans from latest building and find plan with name new_plan_name among them', function(done) {
            request(url)
                .get('/api/buildings/'+buildingIDs[1]+'/plans')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var plans = JSON.parse(res.text);
                    Object.keys(plans).length.should.be.equal(2);
                    var found = false;
                    for (var i in plans){
                        if (plans[i].name == "new_plan_name"){
                            found = true;
                            plans[i].name.should.equal("new_plan_name");
                            plans[i].url.should.equal("url planu2");
                            plans[i].scale.should.equal("2.99999");
                            plans[i].originX.should.equal("0.0000008");
                            plans[i].originY.should.equal("11.888888");
                        }
                    }
                    found.should.be.true();

                    done();
                });
        });
        it('should delete both newly created plans with response code 200 (api key present in headers)', function(done) {
            request(url)
                .delete('/api/buildings/' + buildingIDs[1]+"/plans/new_plan_name")
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    res.status.should.be.equal(200);
                    request(url)
                        .delete('/api/buildings/' + buildingIDs[1]+"/plans/plan_name2")
                        .set({
                            'X-APIKey': '17254faec6a60f58458308763'
                        })
                        .end(function(err, res) {
                            if (err) {
                                throw err;
                            }
                            res.status.should.be.equal(200)
                            done();
                        });
                });
        });

        it('should not find any plans for building', function(done) {
            request(url)
                .get('/api/buildings/'+buildingIDs[1]+'/plans')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var plans = JSON.parse(res.text);
                    Object.keys(plans).length.should.be.equal(0);
                    done();
                });
        });
        it('should create new building with name "Building:" which is short. Should return 400', function(done) {

            var bname = 'Building:';
            var tag = {
                title: bname, //generate random name of building
                tags: ['just_some_tag_to_identify_new_buildings', 'and_another_metatag'],

                location: {
                    "disposition": "fixed",
                    "exposure": "indoor", //this should be fixed by server to outdoor
                    "domain": "" //should be set to physical
                }
            };

            request(url)
                .post('/api/buildings')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(tag)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(400);

                    done();
                });


        });


        it('should get all buildings with meta-tag *just_some_tag_to_identify_new_buildings* which are two buildings', function(done) {
            request(url)
                .get('/api/buildings?tag=just_some_tag_to_identify_new_buildings')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jstags = JSON.parse(res.text);
                    jstags.results.length.should.be.equal(2);
                    done();
                });
        });

        it('should not delete newly created building (api key not in headers) -> response code 401', function(done) {
            request(url)
                .delete('/api/buildings/' + buildingIDs[0])
                                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(401)
                    done();
                });
        });
        it('should delete both newly created buildings with response code 200 (api key present in headers)', function(done) {
            request(url)
                .delete('/api/buildings/' + buildingIDs[0])
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    request(url)
                        .delete('/api/buildings/' + buildingIDs[1])
                        .set({
                            'X-APIKey': '17254faec6a60f58458308763'
                        })
                        .end(function(err, res) {
                            if (err) {
                                throw err;
                            }
                            res.status.should.be.equal(200)
                            done();
                        });
                });
        });

    });


    describe('Anchors', function() {
        it('should get list of all anchors in CSV with response code 200', function(done) {
            request(url)
                .get('/api/anchors')
                .set({
                    'X-ApiKey': '17254faec6a60f58458308763',
                    "Content-Type": 'application/rtlsserver'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    res.status.should.be.equal(200)
                    var localanchorsCount = (res.text.match(/;0x/g) || []).length;
                    localanchorsCount.should.be.equal(anchorsCount);
                    //res.should.be.html;

                    done();
                });
        });
        it('should get list of all anchors in JSON with response code 200', function(done) {
            request(url)
                .get('/api/anchors')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jsanchors = JSON.parse(res.text);
                    jsanchors.results.length.should.be.equal(anchorsCount);
                    done();
                });
        });
        /* xml is deprecated
        it('should get list of all anchors in XML with response code 200', function(done) {
            request(url)
                .get('/api/anchors')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763',
                    "Content-Type": 'application/xml'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var localanchorsCount = (res.text.match(/&lt;environment/g) || []).length;
                    localanchorsCount.should.be.equal(anchorsCount);

                    //res.should.be.html;

                    done();
                });
        });
*/
        it('should create new anchor', function(done) {
            var anchor = {
                title: '0x' + Math.floor(Math.random() * 0xffffffffff).toString(16), //generate random name of anchor
                tags: ['just_some_tag_to_identify_new_anchor', 'and_another_tag'],
                datastreams: [{
                    "id": "battery_level",
                    "current_value": "28%",
                    "symbol": "",
                    "label": "percentage"
                }, {
                    "id": "firmware_version",
                    "current_value": "3.114",
                    "symbol": "",
                    "label": ""
                }, {
                    "id": "hardware_version",
                    "current_value": "2.3",
                    "symbol": "",
                    "label": ""
                }, {
                    "id": "platform",
                    "current_value": "Tag Li-ion",
                    "symbol": "",
                    "label": ""
                }],
                "location": {
                    "disposition": "fixed",
                    "ele": "j",
                    "name": "Building:b",
                    "lat": "0",
                    "exposure": "indoor",
                    "lon": "0",
                    "domain": ""
                }
            };

            request(url)
                .post('/api/anchors')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(anchor)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonAnchor = JSON.parse(res.text);

                    res.status.should.be.equal(200);
                    (jsonAnchor).should.have.ownProperty('feed_id');
                    newAnchorID = jsonAnchor.feed_id;
                    newAnchorID.should.be.above(0);
                    done();
                });


        });
        it('should get newly created anchor in JSON with response code 200', function(done) {
            request(url)
                .get('/api/anchors/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jsanchors = JSON.parse(res.text);
                    jsanchors.tags.length.should.be.equal(2);
                    jsanchors.type.should.be.equal("anchor");
                    jsanchors.datastreams.length.should.be.equal(7); //4 original datastreams + posX+posY+posZ

                    done();
                });
        });
        it('should update newly created anchor in JSON and add another meta-tag', function(done) {
            request(url)
                .put('/api/anchors/' + newAnchorID + "?store=1")
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send({
                    tags: ['just_some_tag_to_identify_new_anchor', 'and_another_tag', 'and_last_tag']
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });

        it('should get all anchors with tag *just_some_tag_to_identify_new_anchor* which is only one anchor', function(done) {
            request(url)
                .get('/api/anchors?tag=just_some_tag_to_identify_new_anchor')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jsanchors = JSON.parse(res.text);
                    jsanchors.results.length.should.be.equal(1);
                    var tagpresent = (res.text.match(/just_some_tag_to_identify_new_anchor/g) || []).length;
                    tagpresent.should.be.equal(1);
                    jsanchors.results[0].tags.length.should.be.equal(3);
                    done();
                });
        });
        it('should not delete newly created anchor (api key not in headers) -> response code 401', function(done) {
            request(url)
                .delete('/api/anchors/' + newAnchorID)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(401)
                    done();
                });
        });
        it('should delete newly created anchor with response code 200 (api key present in headers)', function(done) {
            request(url)
                .delete('/api/anchors/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });

        it('should create new anchor with automatically added datastreams, location and metatags', function(done) {
            var anchor = {
                "title": '0x' + Math.floor(Math.random() * 0xffffffffff).toString(16), //generate random name of anchor
                "tags": [],
                "datastreams": [],
                "location": {}
            };
            request(url)
                .post('/api/anchors')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(anchor)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonAnchor = JSON.parse(res.text);
                    res.status.should.be.equal(200);
                    (jsonAnchor).should.have.ownProperty('feed_id');
                    newAnchorID = jsonAnchor.feed_id;
                    newAnchorID.should.be.above(0);
                    done();
                });
        });
        it('should get newly created anchor in JSON with response code 200 and automatically created datastreams,tags and location', function(done) {
            request(url)
                .get('/api/anchors/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jsanchors = JSON.parse(res.text);
                    jsanchors.tags.length.should.be.equal(1);
                    jsanchors.type.should.be.equal("anchor");
                    jsanchors.datastreams.length.should.be.equal(3); //posX+posY+posZ
                    jsanchors.datastreams[0].id.should.be.equal("posX");
                    jsanchors.datastreams[1].id.should.be.equal("posY");
                    jsanchors.datastreams[2].id.should.be.equal("posZ");
                    jsanchors.datastreams[0].current_value.should.be.equal("N/A");
                    jsanchors.datastreams[1].current_value.should.be.equal("N/A");
                    jsanchors.datastreams[2].current_value.should.be.equal("N/A");

                    done();
                });
        });
        it('should find newly created anchor among all others with automatically created datastreams, tags and location', function(done) {
            var anchors_report;
            request(url)
                .get('/api/anchors')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jsanchors = JSON.parse(res.text);
                    jsanchors.totalResults.should.be.equal(jsanchors.results.length);
                    var found = false;
                    for (var i = 0; i < jsanchors.results.length; i++) {
                        if (jsanchors.results[i].id == newAnchorID) {
                            found = true;
                            jsanchors.results[i].type.should.be.equal("anchor");
                            jsanchors.results[i].datastreams.length.should.be.equal(3); //posX+posY+posZ
                            anchors_report = jsanchors.results[i].datastreams;
                            jsanchors.results[i].datastreams[0].id.should.be.equalOneOf('posX','posY','posZ')
                            jsanchors.results[i].datastreams[1].id.should.be.equalOneOf('posX','posY','posZ')
                            jsanchors.results[i].datastreams[2].id.should.be.equalOneOf('posX','posY','posZ')
                            // jsanchors.results[i].datastreams[0].id.should.be.equal("posX");
                            // jsanchors.results[i].datastreams[1].id.should.be.equal("posY");
                            // jsanchors.results[i].datastreams[2].id.should.be.equal("posZ");
                            jsanchors.results[i].datastreams[0].current_value.should.be.equal("N/A");
                            jsanchors.results[i].datastreams[1].current_value.should.be.equal("N/A");
                            jsanchors.results[i].datastreams[2].current_value.should.be.equal("N/A");
                        }
                    }
                    found.should.be.true();                 
                    done();
                });
        });
    });


    describe('Tags', function() {
        it('should transform latest anchor to tag', function(done) {
            request(url)
                .put('/api/tags/' + newAnchorID + "?store=1")
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send({
                    tags: []
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });
        it('should get newly created transformed tag in JSON with response code 200', function(done) {
            request(url)
                .get('/api/tags/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jstag = JSON.parse(res.text);
                    jstag.type.should.be.equal("tag");
                    done();
                });
        });
        it('should not get newly created tag in /anchors', function(done) {
            request(url)
                .get('/api/anchors/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(404)
                    done();
                });
        });
        it('should delete recently updated tag (api key present in headers)', function(done) {
            request(url)
                .delete('/api/tags/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });
        it('should get list of all tags in CSV with response code 200', function(done) {
            request(url)
                .get('/api/tags')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763',
                    "Content-Type": 'application/rtlsserver'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }

                    res.status.should.be.equal(200)
                    var localtagsCount = (res.text.match(/;0x/g) || []).length;
                    localtagsCount.should.be.equal(tagsCount);
                    //res.should.be.html;

                    done();
                });
        });
        it('should get list of all tags in JSON with response code 200', function(done) {
            request(url)
                .get('/api/tags')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jstags = JSON.parse(res.text);
                    jstags.results.length.should.be.equal(tagsCount);
                    done();
                });
        });/* xml is deprecated
        it('should get list of all tags in XML with response code 200', function(done) {
            request(url)
                .get('/api/tags')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763',
                    "Content-Type": 'application/xml'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var localtagsCount = (res.text.match(/&lt;environment/g) || []).length;
                    localtagsCount.should.be.equal(tagsCount);

                    //res.should.be.html;

                    done();
                });
        });*/

        it('should create new tag with two meta-tags and position [100,100,3]', function(done) {
            var tag = {
                title: '0x' + Math.floor(Math.random() * 0xffffffffff).toString(16), //generate random name of anchor
                description:"zooga wooga text in description",
                tags: ['just_some_tag_to_identify_new_tag', 'and_another_tag'],
                datastreams: [{
                    "id": "posX",
                    "current_value": "100",
                    "symbol": "m",
                    "label": "meters"
                }, {
                    "id": "posY",
                    "current_value": "100",
                    "symbol": "m",
                    "label": "meters"
                }, {
                    "id": "posZ",
                    "current_value": "3",
                    "symbol": "m",
                    "label": "meters"
                }, {
                    "id": "battery_level",
                    "current_value": "28%",
                    "symbol": "",
                    "label": "percentage"
                }, {
                    "id": "firmware_version",
                    "current_value": "3.114",
                    "symbol": "",
                    "label": ""
                }, {
                    "id": "hardware_version",
                    "current_value": "2.3",
                    "symbol": "",
                    "label": ""
                }, {
                    "id": "platform",
                    "current_value": "Tag Li-ion",
                    "symbol": "",
                    "label": ""
                }],
                "location": {
                    "disposition": "fixed",
                    "ele": "j",
                    "name": "Building:b",
                    "lat": "0",
                    "exposure": "indoor",
                    "lon": "0",
                    "domain": ""
                }
            };

            request(url)
                .post('/api/tags')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(tag)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonAnchor = JSON.parse(res.text);
                    res.status.should.be.equal(200);
                    (jsonAnchor).should.have.ownProperty('feed_id');
                    newAnchorID = jsonAnchor.feed_id;
                    newAnchorID.should.be.above(0);
                    done();
                });


        });
        it('should get newly created tag in JSON with response code 200', function(done) {
            request(url)
                .get('/api/tags/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jstag = JSON.parse(res.text);
                    jstag.tags.length.should.be.equal(2); 
                    jstag.type.should.be.equal("tag");

                    done();
                });
        });
        it('should update newly created tag in JSON and add another meta-tag', function(done) {
            request(url)
                .put('/api/tags/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send({
                    tags: ['just_some_tag_to_identify_new_tag', 'and_another_tag', 'and_last_tag']
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });

        it('should get all tags with meta-tag *just_some_tag_to_identify_new_tag* which is only one tag (but count of meta-tags is 3) and position should be [100,100,3]', function(done) {
            request(url)
                .get('/api/tags?tag=just_some_tag_to_identify_new_tag')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jstags = JSON.parse(res.text);
                    jstags.results.length.should.be.equal(1);
                    var tagpresent = (res.text.match(/just_some_tag_to_identify_new_tag/g) || []).length;
                    tagpresent.should.be.equal(1);
                    jstags.results[0].tags.length.should.be.equal(3);
                    parseFloat(jstags.results[0].datastreams.filter(function(datastream) {
                        return datastream.id === "posX";
                    })[0].current_value).should.be.equal(100); //posX
                    parseFloat(jstags.results[0].datastreams.filter(function(datastream) {
                        return datastream.id === "posY";
                    })[0].current_value).should.be.equal(100); //posX
                    parseFloat(jstags.results[0].datastreams.filter(function(datastream) {
                        return datastream.id === "posZ";
                    })[0].current_value).should.be.equal(3); //posX
                    done();
                });
        });
        it('should get all tags with description *zooga* which is only one tag', function(done) {
            request(url)
                .get('/api/tags?q=zooga')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    var isJson = testJSON(res.text);
                    isJson.should.be.true();
                    var jstags = JSON.parse(res.text);
                    jstags.results.length.should.be.equal(1);
                    var tagpresent = (res.text.match(/zooga/g) || []).length;
                    tagpresent.should.be.equal(1);
                    done();
                });
        });
        it('should not delete newly created tag (api key not in headers) -> response code 401', function(done) {
            request(url)
                .delete('/api/tags/' + newAnchorID)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(401)
                    done();
                });
        });
        it('should delete newly created tag with response code 200 (api key present in headers)', function(done) {
            request(url)
                .delete('/api/tags/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });
    });
    describe('Zones', function() {
        it('should create new building for zone/path tests', function(done) {
            var building = {
                "id": "42",
                "alias": "some building",
                "title": "Building:building_name",
                "private": "false",
                "description": "This is some description for my feed.",
                "feed": "Another description in another value",
                "status": "live",
                "updated": "2016-07-29 12:59:43.776046",
                "created": "2016-07-29 12:59:43.776046",
                "creator": "rtlsuser",
                "version": "1.0.0",
                "website": "www.websiteforthisfeed.com",
                "tags": [
                    "brno"
                ],
                "location": {
                    "disposition": "fixed",
                    "ele": "115 meters above sea",
                    "name": "brno",
                    "lat": "14.59",
                    "exposure": "outdoor",
                    "lon": "17.59",
                    "domain": "physical"
                }            
            }
            request(url)
                .post('/api/buildings')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(building)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    buildingID = res.body.id;
                    done();
                });
        });
        it('should create new plan for zone/path tests', function(done) {
            var plan = {
                "name": "plan_name2", //generate random name of building
                "url": "url planu",
                "scale": "10",
                "originX":"0",
                "originY":"11.88"
            };
            request(url)
                .post('/api/buildings/'+buildingID+'/plans')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(plan)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200);
                    //jsonAnchor.should.have.ownProperty('feed_id');

                    done();
                });
        });
        it('should create new zone', function(done) {
            var zone = {
                "points": "21.491 -2.735,21.294 2.205,31.571 2.205,31.571 -2.933,21.491 -2.735",
                "name": "My new zone",
                "type": "danger",
                "zone_settings_reference": none,
                "out_zone_reference": none,
                "prein_zone_reference": none
              }
            request(url)
                .post('/api/buildings/'+buildingID+'/plans/plan_name2/zones')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(zone)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    newZoneID = res.body.id;
                    done();
                });
        });
        it('should edit new zone', function(done) {
            var zone = {
                "name": "Zone 1",
                "type": "warning",
                "points": "21.491 -2.735,21.294 2.205,33.571 2.205,31.571 -2.933,21.491 -2.735"
              }
            request(url)
                .put('/api/zones/'+newZoneID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(zone)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });  
        it('should get new zone', function(done) {
            request(url)
                .get('/api/buildings/'+buildingID+'/plans/plan_name2/zones')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });          
        it('should fail while deleting zone because wrong API', function(done) {
            request(url)
                .delete('/api/zones/'+newZoneID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308762'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(401)
                    done();
                });
        });    
    });                
    describe('Paths', function() {
        it('should get path', function(done) {
            request(url)
                .get('/api/buildings/'+buildingID+'/plans/plan_name2/paths')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });         
        it('should create new path', function(done) {
            var path = {
                "strength": "120",
                "line": "-0.1124 -19.5195,4.4036 -19.5195"
              }
            request(url)
                .post('/api/buildings/'+buildingID+'/plans/plan_name2/paths')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(path)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    newPathID = res.body.id;
                    done();
                });
        });  
        it('should get new path', function(done) {
            request(url)
                .get('/api/buildings/'+buildingID+'/plans/plan_name2/zones')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });   
        it('should edit new path', function(done) {
            var path = {
                "strength": "180",
                "line": "-0.1144 -19.5195,4.4036 -19.5195"
              }            
            request(url)
                .put('/api/paths/'+newPathID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(path)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });                  
        it('should fail while deleting path because wrong API', function(done) {
            request(url)
                .delete('/api/paths/'+newPathID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308762'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(401)
                    done();
                });
        }); 
        it('should delete new path', function(done) {
            request(url)
                .delete('/api/paths/'+newPathID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });      
        it('should delete test plan for zones/paths tests', function(done) {
            request(url)
                .delete('/api/buildings/'+buildingID+'/plans/plan_name2')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });  
        it('should delete test building for zones/paths tests', function(done) {
            request(url)
                .delete('/api/buildings/'+buildingID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });              
    });        
    describe('Restrictions', function() {
        it('should get restrictions and configruations should equal length 16', function(done) {
            request(url)
                .get('/api/restrictions')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    Object.keys(res.body.configuration).length.should.be.equal(16)
                    done();
                });
        }); 
        it('should create new anchor for restriction test', function(done) {
            var anchor = {
                "title": "0x010203040506",
                "tags": ["just_some_tag_to_identify_new_anchor", "and_another_tag"],
                "datastreams": [{
                    "id": "battery_level",
                    "current_value": "28%",
                    "symbol": "",
                    "label": "percentage"
                }, {
                    "id": "firmware_version",
                    "current_value": "3.114",
                    "symbol": "",
                    "label": ""
                }, {
                    "id": "hardware_version",
                    "current_value": "2.3",
                    "symbol": "",
                    "label": ""
                }, {
                    "id": "platform",
                    "current_value": "Tag Li-ion",
                    "symbol": "",
                    "label": ""
                }],
                "location": {
                    "disposition": "fixed",
                    "ele": "j",
                    "name": "Building:b",
                    "lat": "0",
                    "exposure": "indoor",
                    "lon": "0",
                    "domain": ""
                }
            };

            request(url)
                .post('/api/anchors')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(anchor)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonAnchor = JSON.parse(res.text);

                    res.status.should.be.equal(200);
                    (jsonAnchor).should.have.ownProperty('feed_id');
                    newAnchorID = jsonAnchor.feed_id;
                    newAnchorID.should.be.above(0);
                    done();
                });


        });        
        it('should create new anchor slave for restriction test', function(done) {
            var anchor = {
                "title": "0x010203040507",
                "tags": ["just_some_tag_to_identify_new_anchor2", "and_another_tag2"],
                "datastreams": [{
                    "id": "battery_level",
                    "current_value": "46%",
                    "symbol": "",
                    "label": "percentage"
                }, {
                    "id": "firmware_version",
                    "current_value": "3.114",
                    "symbol": "",
                    "label": ""
                }, {
                    "id": "hardware_version",
                    "current_value": "2.3",
                    "symbol": "",
                    "label": ""
                }, {
                    "id": "platform",
                    "current_value": "Tag Li-ion",
                    "symbol": "",
                    "label": ""
                }],
                "location": {
                    "disposition": "fixed",
                    "ele": "j",
                    "name": "Building:b",
                    "lat": "0",
                    "exposure": "indoor",
                    "lon": "0",
                    "domain": ""
                }
            };

            request(url)
                .post('/api/anchors')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(anchor)
                // end handles the response
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    var jsonAnchor = JSON.parse(res.text);

                    res.status.should.be.equal(200);
                    (jsonAnchor).should.have.ownProperty('feed_id');
                    newAnchorID = jsonAnchor.feed_id;
                    newAnchorID.should.be.above(0);
                    done();
                });


        });           
        it('should add new restriction', function(done) {
            var restriction = {
                "slaves":{
                    "0x010203040507":{
                        "slave":"0x801F12571426",
                        "SYNC_FIRSTPATH_THRESHOLD":null,
                        "SYNC_RXPOWERLEVEL_THRESHOLD":null,
                        "SYNC_FIRSTPATH_INDEX_THRESHOLD":null,
                        "BLINK_FIRSTPATH_INDEX_THRESHOLD":null,
                        "BLINK_FIRSTPATH_THRESHOLD":null,
                        "BLINK_RXPOWERLEVEL_THRESHOLD":null}
                    },
                }
            request(url)
                .put('/api/restrictions/0x010203040506')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(restriction)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });   
        it('should get newly created restriction', function(done) {    
            request(url)
                .get('/api/restrictions/0x010203040506')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.body[0].should.be.equal("0x801F12571426");
                    res.status.should.be.equal(200)
                    done();
                });
        });   
        it('should add new incorrect restriction', function(done) {
            var restriction = {
                "slaves":{
                    "0x010203040507":{
                        "slave":"0x801F12571426",
                        "SYNC_FIRSTPATH_THRESHOLD":null,
                        "SYNC_FIRSTPATH_INDEX_THRESHOLD":null,
                        "BLINK_FIRSTPATH_INDEX_THRESHOLD":null,
                        "BLINK_FIRSTPATH_THRESHOLD":null,
                        "BLINK_RXPOWERLEVEL_THRESHOLD":null}
                    },
                }     
            request(url)
                .put('/api/restrictions/0x010203040506')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(restriction)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(400)
                    done();
                });
        });   
        it('should add new incorrect restriction', function(done) {
            var restriction = [
                "string"
            ]   
            request(url)
                .put('/api/restrictions/0x010203040506')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(restriction)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(400)
                    done();
                });
        });              
        it('should delete newly created anchors with its restriction', function(done) {
            request(url)
                .delete('/api/anchors/' + newAnchorID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });                          
    });
    describe('Models', function() {
        it('should add new model', function(done) {
            var models = {
                "id": "42",
                "name": "model_1.obj",
                "vertices": "1,1,1,2,2,2,3,3,3"
              }    
            request(url)
                .post('/api/models')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(models)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    newModelID = res.body.id;
                    done();
                });
        }); 
        it('should get newly created model', function(done) {    
            request(url)
                .get('/api/models')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.body[1].vertices.should.be.equal("1,1,1,2,2,2,3,3,3");
                    res.status.should.be.equal(200)
                    done();
                });
        }); 
        it('should edit newly created model', function(done) {
            var models = {
                "vertices": "1,1,1,2,2,2,3,3,4"
              }     
            request(url)
                .put('/api/models/'+newModelID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(models)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });    
        it('should get newly edited model', function(done) {    
            request(url)
                .get('/api/models')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.body[1].vertices.should.be.equal("1,1,1,2,2,2,3,3,4");
                    res.status.should.be.equal(200)
                    done();
                });
        });  
        it('should delete newly created model', function(done) {    
            request(url)
                .delete('/api/models/'+newModelID)
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    done();
                });
        });                           
    });           
});
