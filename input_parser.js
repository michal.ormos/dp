const readline = require('readline');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

/**
 * Default values
 */
var IP_ADDRESS = "192.168.225.2";
var TAG_RR = 200;
var BLINK_ERROR = 0;
var SYNC_ERROR = 0;

module.exports.getData = function() {
    return [IP_ADDRESS, TAG_RR, BLINK_ERROR, SYNC_ERROR];
}

const QUESTIONS = {
    action1: ['What is the RTLS Studio IP address? [default 192.168.225.2]', 
                'Desired tag refresh rate? [default 200[ms]]', 
                'Desired blink error? [default 0]', 
                'Desired sync error? [default 0]'],
}

let askQuestions = (actionKey) => {
    return new Promise( (res, rej) => {
        let questions = QUESTIONS[actionKey];

        if(typeof questions === 'undefined') rej(`Wrong action key: ${actionKey}`);


        let chainQ = Promise.resolve([]); // resolve to active 'then' chaining (empty array for answers)

        questions.forEach(question => {
          chainQ = chainQ.then( answers => new Promise( (resQ, rejQ) => {
                rl.question(`${question}: `, answer => { answers.push(answer); resQ(answers); });
            })
          );
        });

        chainQ.then((answers) => {
            rl.close();
            res(answers);
        })
    });
};


let handleError = (err) => {
    console.log(`ERROR: ${err}`);
}

function doSomethingwithAnswers(answers, callback) {
    for (answer in answers) {
        if (answers[answer] !== '') {
            switch(answer) {
                case "0":
                    console.log("here");
                    IP_ADDRESS = answers[answer];
                    break;
                case "1":
                    TAG_RR = answers[answer];
                    break;
                case "2":
                    BLINK_ERROR = answers[answer];
                    break;
                case "3":
                    SYNC_ERROR = answers[answer];
                    break;                                                
            }
        }
    }
    // console.log('=================');
    // console.log('You choosed:')
    // console.log("IP Address " + IP_ADDRESS);
    // console.log("Tag refresh rate " + TAG_RR);
    // console.log("Blink error " + BLINK_ERROR);
    // console.log("Sync error " + SYNC_ERROR);

    callback([IP_ADDRESS, TAG_RR, BLINK_ERROR, SYNC_ERROR]);
}

module.exports.main = function main(callback) {
    askQuestions('action1').then(function(result) {
        return doSomethingwithAnswers(result, function(data) {
            callback(data);
        })
    })
    .catch(handleError);
}