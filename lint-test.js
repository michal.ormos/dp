var lint = require('mocha-eslint');

var paths = [
    '/var/www/html/studio/',
  ];
  
  var options = {
      // Specify style of output
      formatter: 'stylish',  // Defaults to `stylish`
      env: {
        browser: true,
        es6: true,
      },
      extends: [
        'airbnb-base',
      ],
      globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
        $: true,
        toastr: true,
        connection: true,
        generalGrid: true,
        summaryGrid: true,
        ControllerGlobalModal: true,
        settingsGrid: true,
        diffgraph: true,
        d3: true,
        ControllerTagConfiguration: true,
        ViewTcpSyncGraph: true,
        ModelTcpSyncGraph: true,
        anchorFwUpgradeGrid: true,
        masterGrid: true,
        anchorStatsGrid: true,
        mapper: true,
        ControllerRegion: true,
        LocalStorage: true,
        Utils: true,
        uwbGrid: true,
        parser: true,
        RTLSManagerControllerRegion: true,
        ModelRegion: true,
        main: true,
        ControllerAnchorsSummary: true,
        io: true,
        ViewSectionalInitialization: true,
        ViewInitialization: true,
        ControllerTagStats: true,
        tcpSyncController: true,
        ControllerDhcp: true,
        tcpSyncGrid: true,
        ModelIndoor: true,
        ModelFeed: true,
        vsn: true,
        ViewMaster: true,
        Controller: true,
        xively: true,
        ControllerGlobalNotifications: true,
        ModelForm: true,
        serverconnect: true,
        parserProcessor: true,
        sensmapconfig: true,
        ControllerMarker: true,
        ControllerRtlsNodestrackingstatistics: true,
        ZoneSensmap: true,
        pixi: true,
        ControllerGlobalSplitter: true,
        PIXI: true,
        Raphael: true,
        sensmapstatus: true,
        ViewLines: true,
        ControllerRtlsZones: true,
        ControllerRtlsRestrictions: true,
        ViewHyperbola: true,
        ViewRestrictions: true,
        ControllerGlobalTutorial: true,
        ControllerGlobalHighlight: true,
        ControllerExplorerLogin: true,
        ViewTagSummary: true,
        ModelTags: true,
        ControllerTagSelector: true,
        ControllerRtlsNodes: true,
        ControllerExplorerBuildings: true,
        ModelQueue: true,
        ControllerProfilerExtendedpositioninformation: true,
        Uri: true,
        ControllerAnchorInitialization: true,
        ControllerGlobalNavigationBar: true,
        ControllerGlobalTabs: true,
        ModelInitialization: true,
        saveAs: true,
        ViewTag: true,
        TWEEN: true,
        Observable: true,
        logController: true,
        ViewRegion: true,
        ModelUrl: true
      },
      parserOptions: {
        ecmaVersion: 2018,
      },
      rules: {
        "indent": ["error", 4, { "SwitchCase": 1 }],
        "quotes": ["error", "double"],
        "no-unneeded-ternary": 0,
        "comma-dangle": ["error", "never"],
        "operator-linebreak": [2, "after", { "overrides": { "?": "before", ":": "before" } }],
        "no-alert": 0,
        "no-plusplus": 0,
        "import/prefer-default-export": 0,
        "max-len": [2, 200],
        "implicit-arrow-linebreak": 0,
        "no-continue": 0,
        "no-console": 0,
        "new-cap": ["error", { "newIsCapExceptions": ["dataAdapter"] }],
        "class-methods-use-this": 0,
        "no-underscore-dangle": 0,
        "no-param-reassign": 0,
        "no-restricted-syntax": 0,
        "object-shorthand": 0
    
      },
  };

describe('ESLint', function() {
    var result = lint(paths, options);
    it('RTLS Web', function (done) {
        var result = lint(paths, options);
        console.log("================================");
        console.log(result);
        done();
      });
});
