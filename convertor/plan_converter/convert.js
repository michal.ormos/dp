var request = require('request');

getBuildings = function(callback) {
    request({
        method:"GET",
        uri: "http://localhost/sensmapserver/api/buildings", 
        headers: {
        'X-ApiKey': "17254faec6a60f58458308763"
            }
        }, function(error, response, body) {
            try {
                var feeds = JSON.parse(body);
                callback(feeds);
            } catch (e) {
                process.exitCode = 1;
                console.error("Error while parsing response from /api/buildings",body);
            }
            
    });
}

addPlan = function(bid, data) { 
  request({
    method: "POST",
    json: true,
    uri: "http://localhost/sensmapserver/api/buildings/" + bid +"/plans",
    headers: {
      'X-ApiKey': "17254faec6a60f58458308763"
    },
    body: data
  }, function(err, resp, body) {

  //  console.log(body);
  })
}

updateBuilding = function(bid, data) { 
 //   console.log("updating building",bid,"with",data);
  request({
    method: "PUT",
    json: true,
    uri: "http://localhost/sensmapserver/api/feeds/" + bid, //USE /feeds/, because "/buildings/" would keep the plan tags
    headers: {
      'X-ApiKey': "17254faec6a60f58458308763"
    },
    body: data
  }, function(err, resp, body) {

  //  console.log(body);
  })
}

escapeRegExp = function (str) {
    
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

parseBuildingTags = function(feed) {

    var plans = { "id": feed.id };
    for (var t in feed.tags) {
        if (feed.tags[t].indexOf("plan:#") == 0) {
            var myPlan = /plan:#(.+)#:URL:#(.+)#/g.exec(feed.tags[t]);
            if (myPlan == null || myPlan === undefined || myPlan[1] == undefined || myPlan[2] == undefined) {
                console.log("corrupted building structure:",feed.tags[t]);
                continue;
            }
            var plan = { 'name': myPlan[1], 'url': myPlan[2], 'raw': feed.tags[t],"building_id":feed.id };
            plans[myPlan[1]] = plan;
        }
    }
    for (var p in plans) {
        if (p === "id") {
            continue;
        }
        for (var t in feed.tags) {
            //#2#_scale:#662.6920104934504
            //#2#_zeroaxis:(#2166.614457831325_#561.714859437751)

            var regex = new RegExp("#(" + escapeRegExp(plans[p].name) + ")#_scale:#(.+)", "g");
            var regexForAxis = new RegExp("#(" + escapeRegExp(plans[p].name) + ")#_zeroaxis:\\(#(.+)_#(.+)\\)", "g");

            var output = regex.exec(feed.tags[t]);
            if (output) {
                plans[output[1]].scale = output[2];
                plans[output[1]].scaleRaw = feed.tags[t];

            }
            var zeroaxis = regexForAxis.exec(feed.tags[t]);
            if (zeroaxis) {
                var axis = {};
                axis.x = zeroaxis[2];
                axis.y = zeroaxis[3];
                plans[zeroaxis[1]].originX = axis.x;
                plans[zeroaxis[1]].originY = axis.y;
                plans[zeroaxis[1]].originRaw = feed.tags[t];
            }
        }
    }

    return plans;

}

removeTags = function (tags,plans){
    //var newTags = tags;
    for (var plan in plans){
        var planRaw = plans[plan].raw;
        var originRaw = plans[plan].originRaw;
        var scaleRaw = plans[plan].scaleRaw;
        

        var index1 = tags.indexOf(planRaw);
        if (index1 > -1) {
            tags.splice(index1, 1);
        }

        var index2 = tags.indexOf(originRaw);

        if (index2 > -1) {
            tags.splice(index2, 1);
        }
        var index3 = tags.indexOf(scaleRaw);

        if (index3 > -1) {
            tags.splice(index3, 1);
        }
    }
    return tags;
}

getBuildings(function(buildings){
    if (buildings && buildings.results && buildings.results.length){
        for (var i = 0; i < buildings.results.length; i++) { 
            if (buildings.results[i].id !== undefined && buildings.results[i].tags){
             ////   console.log("------------- NEXT ITEM ----------- ")
                var plans = parseBuildingTags(buildings.results[i]);
             ////   console.log("OLD TAGS:",buildings.results[i].tags )

                var newTags = removeTags(buildings.results[i].tags,plans);
                updateBuilding(buildings.results[i].id,{'tags':newTags, 'type':'building'});
             ////   console.log("NEW TAGS:", newTags);
                for (var plan in plans){
                    //console.log(plans[plan]);
                    if (plans[plan].url !== undefined && 
                        plans[plan].name !== undefined && 
                        plans[plan].originY !== undefined && 
                        plans[plan].originX  !== undefined && 
                        plans[plan].scale  !== undefined){
                        addPlan(buildings.results[i].id, plans[plan]);
                    }
                }
            } else {
                console.log("INFO: NO TAGS DETECTED FOR BUILDING",buildings.results[i].id);
            }
        }
    } else {
        console.log("INFO: NO BUILDINGS DETECTED");
    }
})