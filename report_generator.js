var binary = require('binary');
var crc = require('crc');
var net = require('net');

var getRandomInt = function(max) {
	var random = Math.floor(Math.random() * Math.floor(max));
	return random;
}

/**
 * buffer: 23 
 		   9c92 < CRC
 		   3d00 < len
 		   66 fe 61 39 80 d8 <anchor mac
 		   55                < report type ascii (55 = U as universal)
 		   02 20 00          <report universal header (2 blink)
 		   bb                <- fcode
 		   ea 27 f0 5f 20 00 <- tag MAC
 		   13                <- seq (19)
 		   de12acde0d000000ca0bf6378800e933d535be18f6000ebb
 		   070400 <report universal header 
 		   ab308096 <anchor barometer
 		   030900 <report universal header
 		   0b7f7785960fff0281 <-extendend blink
 * parsed: [ { report: '|Presence|d8803961fe66|A|00205ff027ea|bb|000|19|0.932279049948668|0|3018|136|6334|748.14|-81.99|-80.37|0b7f7785960fff0281|ab308096|',
*/
/*

23 9c92 3d00 66fe613980d855022000 bb ea27f05f2000 13 de12acde0d000000ca0bf6378800e933d535be18f6000ebb070400ab308096 030900 0b7f7785960fff0281
23 3907 5800 66fe613980d855022000 bb ea27f05f2000 46 74d8ca64ef000000ad078f385400e1373130b01dfa00a4ba070400a9c99f96 032400 08c90104070376070105011101000164000000757575750f800000000300000000e80303
*/



var createReportBuffer = function (fcode, anchorMacString, tagMacString, sequenceNumber, anchorBaroString, extBlinkString) {
	var anchorAddrB = new Buffer([0x60+getRandomInt(7),0xfe,0x61,0x39,0x80,0xd8]);
	var universalType = new Buffer([0x55]);
	var blinkUniversalHeader = new Buffer([0x02,0x20,0x00]);
	var fcodeB = new Buffer([0xbb]);
	var tagAddrB = new Buffer(tagMacString, 'hex');
	var seqNumberB = new Buffer([sequenceNumber]);
	var uwbPayloadB = new Buffer([0xde,0x12,0xac,0xde,0x0d,0x00,0x00,0x00,0xca,0x0b,0xf6,0x37,0x88,0x00,0xe9,0x33,0xd5,0x35,0xbe,0x18,0xf6,0x00,0x0e,0xbb]);
	var anchorBaroHeaderB = new Buffer([0x07, 0x04, 0x00]);
	var anchorBaroB = new Buffer([0xab, 0x30, 0x80, 0x96]);
	var tagBaroHeaderB = new Buffer([0x03, 0x09, 0x00]);
	var tagBaroB = new Buffer([0x0b,0x7f,0x77,0x85,0x96,0x0f,0xff,0x02,0x81]);

	var extInfoHeaderB = new Buffer([0x03, 0x24, 0x00])
	var extInfoB = new Buffer([0x08, 0xc9, 0x01, 0x04, 0x07, 0x03, 0x76, 0x07, 0x01, 0x05, 0x01, 0x11, 0x01, 0x00, 0x01, 0x64, 0x00, 0x00, 0x00, 0x75, 0x75, 0x75, 0x75, 0x0f,
							   0x80, 0x00, 0x00, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0xe8, 0x03, 0x03])

	/*var buf = new Buffer([0x23,
						0x9c,0x92,
						0x3d,0x00,
						0x66,0xfe,0x61,0x39,0x80,0xd8,
						0x55,
						0x02,0x20,0x00,
						fcode,
						0xea,0x27,0xf0,0x5f,0x20,0x00,
						0x13,
						0xde,0x12,0xac,0xde,0x0d,0x00,0x00,0x00,0xca,0x0b,0xf6,0x37,0x88,0x00,0xe9,0x33,0xd5,0x35,0xbe,0x18,0xf6,0x00,0x0e,0xbb,
						0x07,0x04,0x00,
						0xab,0x30,0x80,0x96,
						0x03,0x09,0x00,
						0x0b,0x7f,0x77,0x85,0x96,0x0f,0xff,0x02,0x81]);*/
	return Buffer.concat([anchorAddrB, universalType, 
						blinkUniversalHeader, fcodeB, tagAddrB, seqNumberB, uwbPayloadB, 
						anchorBaroHeaderB, anchorBaroB, 
						sequenceNumber < 3 ? extInfoHeaderB : tagBaroHeaderB, //for first three messages send info blink
						sequenceNumber < 3 ? extInfoB       : tagBaroB]); // otherwise barometer
}

var numberOfTags = 5;
var tcpclients = [];
for (i = 0; i < numberOfTags; i++) {
	tcpclients.push(new net.Socket());
}
var numberOfMessagesPerTag = 1300;
var delimB = new Buffer([0x23]);

for (var j = 0; j < numberOfTags; j++) {
	tcpclients[j].connect(5000, "172.16.18.20", function() {
			for (var i = 0; i < numberOfMessagesPerTag; i++) {
				var report = createReportBuffer(
							0xbb,
							"d8803961fe6" + getRandomInt(7), //jump to 6 anchors with last byte 0,1,2,3,4,5,6
							("000000000000" + this.tagIndex.toString('16')).slice(-12)  ,
							i, //sequence number
							"ab308096", //anchor barometer
							"0b7f7785960fff0281" //extended blink					
						)
				var lenB = new Buffer((report.length.toString('16') + "0000").substr(0, 4), 'hex'); 
				var reportWithLength = Buffer.concat([lenB, report]);
				var crcInt = crc.crc16ccitt(reportWithLength);
				var crcB = new Buffer([(crcInt & 0xFF), ((crcInt >> 8) & 0xFF)])

				var message = Buffer.concat([delimB, crcB, reportWithLength]);
				//console.log(message.toString('hex'));
				setTimeout(function(connector, message, tagIndex, messageIndex){
					console.log("tag"+tagIndex +"message "+messageIndex+" :" + message.toString('hex'));
					connector.write(message);

				}, i*200 + getRandomInt(10) /*N ms apart*/, this.connector, message, this.tagIndex, i);
			}
	}.bind({connector: tcpclients[j], tagIndex: j}));
}

