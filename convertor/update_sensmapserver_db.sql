--
-- Create procedures
--
drop procedure if exists firstentry;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `firstentry`(IN `lid` INT(11), IN `rid` INT(11), IN `fromtime` TIMESTAMP(6), IN `totime` TIMESTAMP(6), OUT `value` TIMESTAMP(6))
    NO SQL
begin
	declare done int default false;
	declare polygon geometry;
	declare time timestamp(6);
	declare coord point;

	declare cur1 cursor for select position_history.at, position_history.position from position_history where feed_reference = lid and at < totime and at > fromtime order by at asc;
	declare cur2 cursor for select zones.polygon from zones where zones.id = rid;

	declare continue handler for not found set done = true;

	open cur2;
	fetch cur2 into polygon;
	open cur1;

	read_loop: loop
		fetch cur1 into time, coord;
		
		if done then
			set value = null;
			leave read_loop;
		end if;

		if st_contains(polygon, coord) then
			set value = time;
			leave read_loop;
		end if;
	end loop;
end$$
DELIMITER ;

drop procedure if exists lastexit;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `lastexit`(IN `lid` INT(11), IN `rid` INT(11), IN `fromtime` TIMESTAMP(6), IN `totime` TIMESTAMP(6), OUT `value` TIMESTAMP(6))
    NO SQL
begin
	declare done int default false;
	declare polygon geometry;
	declare time timestamp(6);
	declare coord point;

	declare cur1 cursor for select position_history.at, position_history.position from position_history where feed_reference = lid and at < totime and at > fromtime order by at desc;
	declare cur2 cursor for select zones.polygon from zones where zones.id = rid;

	declare continue handler for not found set done = true;

	open cur2;
	fetch cur2 into polygon;
	open cur1;

	read_loop: loop
		fetch cur1 into time, coord;
		
		if done then
			set value = null;
			leave read_loop;
		end if;

		if st_contains(polygon, coord) then
			set value = time;
			leave read_loop;
		end if;
	end loop;
end$$
DELIMITER ;

drop procedure if exists timein;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `timein`(IN `lid` INT(11), IN `rid` INT(11), IN `fromtime` TIMESTAMP(6), IN `totime` TIMESTAMP(6), OUT `value` INT(20))
begin
	declare inside int default false;
    declare done int default false;
	
	declare polygon geometry;
	declare firsttime timestamp(6);
	declare lasttime timestamp(6);
	declare time timestamp(6);
	declare coord point;

	declare cur1 cursor for select position_history.at, position_history.position from position_history where feed_reference = lid and at >= fromtime and at <= totime order by at asc;
	declare cur2 cursor for select zones.polygon from zones where zones.id = rid;

	declare continue handler for not found set done = true;
	
	set value = 0;

	open cur1;
	open cur2;
	fetch cur2 into polygon;

	read_loop: loop
		fetch cur1 into time, coord;

		if done then
			if inside then
				set value = value + TIMESTAMPDIFF(second, firsttime, lasttime);
			end if;

			leave read_loop;
		end if;

		if ST_CONTAINS(polygon, coord) then
			if not inside then
				set inside = true;
				set firsttime = time;
				set lasttime = time;
			else
				set lasttime = time;
			end if;
		else
			if inside then
				set value = value + TIMESTAMPDIFF(second, firsttime, lasttime);
				set inside = false;
			end if;
		end if;
	end loop;
end$$
DELIMITER ;

-- -----------------------------
-- Create new tables
-- -----------------------------	

/*!40101 SET character_set_client = utf8mb4 */;		
CREATE TABLE IF NOT EXISTS `models` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `vertices` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `feed_zone` (    
  `id` int(11) NOT NULL AUTO_INCREMENT,   
  `feed_id` int(11) NOT NULL,   
  `zone_id` int(11) NOT NULL,   
  `at` datetime(6) NOT NULL,    
  `status` varchar(255) NOT NULL,   
  PRIMARY KEY (`id`),   
  UNIQUE KEY `feed_id` (`feed_id`,`zone_id`),   
  KEY `zone_id` (`zone_id`),    
  CONSTRAINT `feed_zone_ibfk_1` FOREIGN KEY (`feed_id`) REFERENCES `feeds` (`id`) ON DELETE CASCADE,    
  CONSTRAINT `feed_zone_ibfk_2` FOREIGN KEY (`zone_id`) REFERENCES `zones` (`id`) ON DELETE CASCADE   
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;    

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `parent` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `parent` (`parent`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=2 ;


CREATE TABLE IF NOT EXISTS `grids` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `configuration` text NOT NULL,
  `at` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `feed_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `feed` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `feed` (`feed`),
  KEY `category` (`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `position_history` (
  `position_id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_reference` int(20) DEFAULT NULL,
  `position` point DEFAULT NULL,
  `posZ` varchar(32) DEFAULT NULL,
  `clr` varchar(16) DEFAULT NULL,
  `numberOfAnchors` varchar(16) DEFAULT NULL,
  `at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
   PRIMARY KEY (`position_id`)
  ) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(256) NOT NULL,
  `description` varchar(2048) NOT NULL,
  `at` timestamp(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `checked` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=2;

CREATE TABLE IF NOT EXISTS `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto generated unique ID which identifies zone.',
  `name` varchar(255) DEFAULT NULL COMMENT 'Name of zone.',
  `type` varchar(255) DEFAULT NULL COMMENT 'Four possible values: info, warning, danger and valid. You can base your events on this type, e.g. red popup for danged zone, yellow for warning, etc.',
  `building_reference` int(11) NOT NULL COMMENT 'Feed ID of the building where is this zone.',
  `polygon` geometry NOT NULL COMMENT 'List of comma separated X Y values representing vertices of polygon in meters.',
  `plan_name` varchar(191) NOT NULL COMMENT 'Name of plan where the zone is located.',
  `scale` float NOT NULL COMMENT 'How many pixels is one meter for given plan.',
  `originX` float NOT NULL COMMENT 'Where is the origin X point for given plan.',
  `originY` float NOT NULL COMMENT 'Where is the origin Y point for given plan.',
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `zones_history`;

CREATE TABLE IF NOT EXISTS `ds_history` (
  `history_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto generated unique ID which identifies datastream history.',
  `value` varchar(255) NOT NULL COMMENT 'Value of datastream''s phenomenom at time ''at''.',
  `datastream_reference` int(11) NOT NULL COMMENT 'Foregin key of datastream from table datastreams.',
  `at` timestamp(6) NULL DEFAULT NULL COMMENT 'Time of ''value''.',
  PRIMARY KEY (`history_id`),
  KEY `datastream_reference` (`datastream_reference`),
  CONSTRAINT `ds_history_ibfk_1` FOREIGN KEY (`datastream_reference`) REFERENCES `datastreams` (`datastream_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4;

CREATE TABLE IF NOT EXISTS `initialization` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `initialization_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `initialization_reference` int(11) NOT NULL,
  `master` varchar(255) NOT NULL,
  `anchors` text NOT NULL,
  `overallscore` varchar(31) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `initialization_reference` (`initialization_reference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `paths` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Auto generated unique ID which identifies path.',
  `building_reference` int(11) NOT NULL,
  `plan_name` varchar(191) NOT NULL,
  `line` linestring NOT NULL,
  `strength` float NOT NULL COMMENT 'Path radius in meters.',
  `scale` float NOT NULL COMMENT 'How many pixels is one meter for given plan.',
  `originX` float NOT NULL COMMENT 'Where is the origin X point of given plan.',
  `originY` float NOT NULL COMMENT 'Where is the origin Y point of given plan.',
  PRIMARY KEY (`id`),
  KEY `building_reference` (`building_reference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `plans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) NOT NULL,
  `originX` varchar(512) NOT NULL,
  `originY` varchar(512) NOT NULL,
  `scale` varchar(512) NOT NULL,
  `url` varchar(1024) NOT NULL,
  `building_reference` int(11) NOT NULL,
  `model_reference` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`,`building_reference`),
  KEY `building_reference` (`building_reference`),
  KEY `model_reference` (`model_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `restriction_masters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `restriction_slaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `master_reference` int(11) NOT NULL,
  `slave` varchar(255) NOT NULL,
  KEY `master_reference` (`master_reference`),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 AUTO_INCREMENT=1 ;


CREATE TABLE IF NOT EXISTS `exits` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_reference` int(11) NOT NULL,
  `line` linestring NOT NULL,
  `strength` float NOT NULL,
  `scale` float NOT NULL,
  `originX` float NOT NULL,
  `originY` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_reference` (`plan_reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `tag_barometer` (
  `mac_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `reference_anchor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `offset` float NOT NULL,
  PRIMARY KEY (`mac_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `anchor_barometer` (
  `mac_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pressure` float NOT NULL,
  `temperature` float NOT NULL,
  `height` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `plan_reference` int(11) DEFAULT NULL,
  PRIMARY KEY (`mac_address`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `walls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `plan_reference` int(11) NOT NULL,
  `line` linestring NOT NULL,
  `strength` float NOT NULL,
  `scale` float NOT NULL,
  `originX` float NOT NULL,
  `originY` float NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plan_reference` (`plan_reference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `configuration` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=2 ;


CREATE TABLE IF NOT EXISTS `restriction_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `STORE_TO_DB` tinyint(4) DEFAULT NULL,
  `ZONE_TOLERANCE` float UNSIGNED DEFAULT NULL,
  `IGNORE_BORDER_POSITIONS` tinyint(4) DEFAULT NULL,
  `Z_AXIS_COMPENSATION` float DEFAULT NULL,
  `MPFIT_BESTNORM_THRESHOLD` float UNSIGNED DEFAULT NULL,
  `OPTIMUM_ERROR_THRESHOLD` float UNSIGNED DEFAULT NULL,
  `CORRECTED_OPTIMUM_ERROR_PERCENTAGE` float UNSIGNED DEFAULT NULL,
  `MAX_NUMBER_OF_COMPUTING_ANCHORS` int(10) UNSIGNED DEFAULT NULL,
  `MIN_NUMBER_OF_COMPUTING_ANCHORS` int(10) UNSIGNED DEFAULT NULL,
  `MIN_NUMBER_OF_CORRECTING_ANCHORS` int(10) UNSIGNED DEFAULT NULL,
  `SYNC_FIRSTPATH_THRESHOLD` float DEFAULT NULL,
  `SYNC_RXPOWERLEVEL_THRESHOLD` float DEFAULT NULL,
  `SYNC_FIRSTPATH_INDEX_THRESHOLD` float DEFAULT NULL,
  `BLINK_FIRSTPATH_INDEX_THRESHOLD` float DEFAULT NULL,
  `BLINK_FIRSTPATH_THRESHOLD` float DEFAULT NULL,
  `BLINK_RXPOWERLEVEL_THRESHOLD` float DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `profiler` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mac_address` varchar(14) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `zones_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `exclusive` tinyint(1) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `zones_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `in_count` int(11) NOT NULL,
  `in_interval` float DEFAULT NULL,
  `out_count` int(11) NOT NULL,
  `out_interval` float DEFAULT NULL,
  `prein_interval` float DEFAULT NULL,
  `out_timeout` float DEFAULT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `zone_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone_reference` int(11) NOT NULL,
  `zone_group_reference` int(11) NOT NULL,
   PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

--
-- Insert new rows if not exist
--
INSERT IGNORE INTO `models` (`id`, `name`, `vertices`) VALUES (1, 'arrow', '0.04167785868048668,-0.025539522990584373,-0.0781799703836441,0.04167785868048668,-0.025539522990584373,0.19603902101516724,-0.04318675026297569,-0.025539522990584373,-0.0781799703836441,0.04167785868048668,-0.025539522990584373,0.19603902101516724,-0.043186888098716736,-0.025539522990584373,0.19603902101516724,-0.04318675026297569,-0.025539522990584373,-0.0781799703836441,0.04167799651622772,0.0423775315284729,-0.07817984372377396,-0.043186888098716736,0.0423775315284729,-0.0781799703836441,0.04167785868048668,0.0423775315284729,0.19603917002677917,-0.043186888098716736,0.0423775315284729,-0.0781799703836441,-0.043186888098716736,0.0423775315284729,0.19603902101516724,0.04167785868048668,0.0423775315284729,0.19603917002677917,0.04167785868048668,-0.025539522990584373,-0.0781799703836441,0.04167799651622772,0.0423775315284729,-0.07817984372377396,0.04167785868048668,-0.025539522990584373,0.19603902101516724,0.04167799651622772,0.0423775315284729,-0.07817984372377396,0.04167785868048668,0.0423775315284729,0.19603917002677917,0.04167785868048668,-0.025539522990584373,0.19603902101516724,0.04167785868048668,-0.025539522990584373,0.19603902101516724,0.04167785868048668,0.0423775315284729,0.19603917002677917,-0.043186888098716736,-0.025539522990584373,0.19603902101516724,0.04167785868048668,0.0423775315284729,0.19603917002677917,-0.043186888098716736,0.0423775315284729,0.19603902101516724,-0.043186888098716736,-0.025539522990584373,0.19603902101516724,-0.043186888098716736,-0.025539522990584373,0.19603902101516724,-0.043186888098716736,0.0423775315284729,0.19603902101516724,-0.04318675026297569,-0.025539522990584373,-0.0781799703836441,-0.043186888098716736,0.0423775315284729,0.19603902101516724,-0.043186888098716736,0.0423775315284729,-0.0781799703836441,-0.04318675026297569,-0.025539522990584373,-0.0781799703836441,0.04167799651622772,0.0423775315284729,-0.07817984372377396,0.04167785868048668,-0.025539522990584373,-0.0781799703836441,0.1662469208240509,0.042377665638923645,-0.09471921622753143,0.04167785868048668,-0.025539522990584373,-0.0781799703836441,0.16624677181243896,-0.025539522990584373,-0.09471935778856277,0.1662469208240509,0.042377665638923645,-0.09471921622753143,-0.1661357283592224,0.042377665638923645,-0.09282957762479782,0.1662469208240509,0.042377665638923645,-0.09471921622753143,-0.018470844253897667,0.042377665638923645,-0.215237095952034,0.1662469208240509,0.042377665638923645,-0.09471921622753143,0.02013576403260231,0.042377665638923645,-0.21687939763069153,-0.018470844253897667,0.042377665638923645,-0.215237095952034,-0.043186888098716736,0.0423775315284729,-0.0781799703836441,0.04167799651622772,0.0423775315284729,-0.07817984372377396,-0.1661357283592224,0.042377665638923645,-0.09282957762479782,0.04167799651622772,0.0423775315284729,-0.07817984372377396,0.1662469208240509,0.042377665638923645,-0.09471921622753143,-0.1661357283592224,0.042377665638923645,-0.09282957762479782,-0.04318675026297569,-0.025539522990584373,-0.0781799703836441,-0.043186888098716736,0.0423775315284729,-0.0781799703836441,-0.1661357283592224,-0.025539522990584373,-0.09282957762479782,-0.043186888098716736,0.0423775315284729,-0.0781799703836441,-0.1661357283592224,0.042377665638923645,-0.09282957762479782,-0.1661357283592224,-0.025539522990584373,-0.09282957762479782,0.04167785868048668,-0.025539522990584373,-0.0781799703836441,-0.04318675026297569,-0.025539522990584373,-0.0781799703836441,0.16624677181243896,-0.025539522990584373,-0.09471935778856277,-0.04318675026297569,-0.025539522990584373,-0.0781799703836441,-0.1661357283592224,-0.025539522990584373,-0.09282957762479782,0.16624677181243896,-0.025539522990584373,-0.09471935778856277,0.02013576403260231,0.042377665638923645,-0.21687939763069153,0.02013576403260231,-0.02553938701748848,-0.21687939763069153,-0.018470844253897667,0.042377665638923645,-0.215237095952034,0.02013576403260231,-0.02553938701748848,-0.21687939763069153,-0.018470844253897667,-0.02553938701748848,-0.215237095952034,-0.018470844253897667,0.042377665638923645,-0.215237095952034,0.1662469208240509,0.042377665638923645,-0.09471921622753143,0.16624677181243896,-0.025539522990584373,-0.09471935778856277,0.02013576403260231,0.042377665638923645,-0.21687939763069153,0.16624677181243896,-0.025539522990584373,-0.09471935778856277,0.02013576403260231,-0.02553938701748848,-0.21687939763069153,0.02013576403260231,0.042377665638923645,-0.21687939763069153,-0.1661357283592224,-0.025539522990584373,-0.09282957762479782,-0.1661357283592224,0.042377665638923645,-0.09282957762479782,-0.018470844253897667,-0.02553938701748848,-0.215237095952034,-0.1661357283592224,0.042377665638923645,-0.09282957762479782,-0.018470844253897667,0.042377665638923645,-0.215237095952034,-0.018470844253897667,-0.02553938701748848,-0.215237095952034,0.16624677181243896,-0.025539522990584373,-0.09471935778856277,-0.1661357283592224,-0.025539522990584373,-0.09282957762479782,0.02013576403260231,-0.02553938701748848,-0.21687939763069153,-0.1661357283592224,-0.025539522990584373,-0.09282957762479782,-0.018470844253897667,-0.02553938701748848,-0.215237095952034,0.02013576403260231,-0.02553938701748848,-0.21687939763069153');


INSERT IGNORE INTO `category` (`id`, `name`, `parent`) VALUES (1, 'root', NULL);

DELETE FROM notifications WHERE checked IS NULL AND code='NEW_VERSION';

INSERT IGNORE INTO `notifications` (`code`, `description`, `checked`) VALUES
('NEW_VERSION', 'RTLS Studio has been updated to newer version. <a href="/documentation/changelog"> Click here to read changelog</a>', NULL);

INSERT IGNORE INTO `configuration` (`id`, `region`) VALUES
(1, 'other');

DELETE FROM notifications WHERE checked IS NULL AND code='EULA';

INSERT IGNORE INTO `notifications` (`code`, `description`, `checked`) VALUES
('EULA', 'EULA confirmation needed', 1);

-- ----------------------------------------------------------
-- drop old unnecessary columns and add new columns
-- ----------------------------------------------------------

drop procedure if exists schema_change;
delimiter ';;'
create procedure schema_change() begin
    /* delete columns if they exist */
    if EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'datastreams' AND column_name = 'alert_level') then
        alter table sensmapserver.datastreams drop column `alert_level`;
    end if;
    if EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'keys' AND column_name = 'friend_invitation') then
        alter table sensmapserver.keys drop column `friend_invitation`;
    end if;
    if EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'users' AND column_name = 'telecalm_user_reference') then
        alter table sensmapserver.users drop column `telecalm_user_reference`;
    end if;    
    if EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'feeds' AND column_name = 'status') then
        alter table sensmapserver.feeds drop column `status`;
    end if;
    if EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'zones' AND column_name = 'feed_id') then
         ALTER TABLE `zones` CHANGE `feed_id` `building_reference` INT(11);
    end if;
    if EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'zones' AND column_name = 'plan') then
        ALTER TABLE `zones` CHANGE `plan` `plan_name` varchar(191);
    end if;
    if EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'feeds' AND column_name = 'feed') then
        ALTER TABLE `sensmapserver`.`feeds` CHANGE `feed` `feed` varchar(11);
    end if;
    if EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'notifications' AND column_name = 'at') then
        ALTER TABLE `sensmapserver`.`notifications` CHANGE `at` `at` timestamp(6) DEFAULT CURRENT_TIMESTAMP(6);
    end if;
    /* add columns if they do not exist */
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'position_history' AND column_name = 'plan_reference') then
        alter table sensmapserver.position_history ADD COLUMN `plan_reference` int(11) DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'feeds' AND column_name = 'model_reference') then
      alter table sensmapserver.feeds ADD COLUMN `model_reference` int(11) DEFAULT NULL;
    end if;    
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'feeds' AND column_name = 'type') then
      alter table sensmapserver.feeds ADD COLUMN `type` varchar(255) DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'plans' AND column_name = 'height') then
      alter table sensmapserver.plans ADD COLUMN `height` double DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'plans' AND column_name = 'elevation') then
      alter table sensmapserver.plans ADD COLUMN `elevation` double DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'notifications' AND column_name = 'updated_at') then
      alter table sensmapserver.notifications ADD COLUMN `updated_at` timestamp(6) DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6);
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'notifications' AND column_name = 'severity') then
      alter table sensmapserver.notifications ADD COLUMN `severity` int(11);
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'configuration' AND column_name = 'tutorial') then
      alter table sensmapserver.configuration ADD COLUMN `tutorial` tinyint(1) DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'initialization_detail' AND column_name = 'position') then
      alter table sensmapserver.initialization_detail ADD COLUMN `position` varchar(255) DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'restriction_masters' AND column_name = 'settings_reference') then
      alter table sensmapserver.restriction_masters ADD COLUMN `settings_reference` int(11) DEFAULT NULL;
    end if;

    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'restriction_slaves' AND column_name = 'SYNC_FIRSTPATH_THRESHOLD') then
      alter table sensmapserver.restriction_slaves ADD COLUMN `SYNC_FIRSTPATH_THRESHOLD` float DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'restriction_slaves' AND column_name = 'SYNC_RXPOWERLEVEL_THRESHOLD') then
      alter table sensmapserver.restriction_slaves ADD COLUMN `SYNC_RXPOWERLEVEL_THRESHOLD` float DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'restriction_slaves' AND column_name = 'SYNC_FIRSTPATH_INDEX_THRESHOLD') then
      alter table sensmapserver.restriction_slaves ADD COLUMN `SYNC_FIRSTPATH_INDEX_THRESHOLD` float DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'restriction_slaves' AND column_name = 'BLINK_FIRSTPATH_INDEX_THRESHOLD') then
      alter table sensmapserver.restriction_slaves ADD COLUMN `BLINK_FIRSTPATH_INDEX_THRESHOLD` float DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'restriction_slaves' AND column_name = 'BLINK_FIRSTPATH_THRESHOLD') then
      alter table sensmapserver.restriction_slaves ADD COLUMN `BLINK_FIRSTPATH_THRESHOLD` float DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'restriction_slaves' AND column_name = 'BLINK_RXPOWERLEVEL_THRESHOLD') then
      alter table sensmapserver.restriction_slaves ADD COLUMN `BLINK_RXPOWERLEVEL_THRESHOLD` float DEFAULT NULL;
    end if;

    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'zones' AND column_name = 'zone_settings_reference') then
      alter table sensmapserver.zones ADD COLUMN `zone_settings_reference` int(11) DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'zones' AND column_name = 'out_zone_reference') then
      alter table sensmapserver.zones ADD COLUMN `out_zone_reference` int(11) DEFAULT NULL;
    end if;
    if NOT EXISTS (SELECT * FROM information_schema.columns WHERE table_schema ='sensmapserver' AND table_name = 'zones' AND column_name = 'prein_zone_reference') then
      alter table sensmapserver.zones ADD COLUMN `prein_zone_reference` int(11) DEFAULT NULL;
    end if;
end;;

use sensmapserver

delimiter ';'
call schema_change();

drop procedure if exists schema_change;

-- --------------------------------
-- Modify existing table structure
-- --------------------------------

ALTER TABLE category MODIFY name VARCHAR(191);
ALTER TABLE plans MODIFY name VARCHAR(191);
ALTER TABLE feeds MODIFY title VARCHAR(191);


ALTER DATABASE `sensmapserver` CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

-- --------------------------------------------------------------------
-- set encoding to utf 8, keep rest (mainly position_history and ds_history at latin1, to save space)
-- --------------------------------------------------------------------

ALTER TABLE `feeds` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `datastreams` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `zones` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `category` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `grids` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `models` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `paths` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `plans` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `tags` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;
ALTER TABLE `users` convert to character SET utf8mb4 collate utf8mb4_unicode_ci;

-- -----------------------------
-- Create new index 
-- -----------------------------

DELIMITER $$

DROP PROCEDURE IF EXISTS `sensmapserver`.`CreateIndex` $$
CREATE PROCEDURE `sensmapserver`.`CreateIndex`
(
    given_type     VARCHAR(64),
    given_database VARCHAR(64),
    given_table    VARCHAR(64),
    given_index    VARCHAR(64),
    given_columns  VARCHAR(64)
)
BEGIN

    DECLARE IndexIsThere INTEGER;

    SELECT COUNT(1) INTO IndexIsThere
    FROM INFORMATION_SCHEMA.STATISTICS
    WHERE table_schema = given_database
    AND   table_name   = given_table
    AND   index_name   = given_index;

    IF IndexIsThere = 0 THEN
        SET @sqlstmt = CONCAT('CREATE ',given_type,' INDEX ',given_index,' ON ',
        given_database,'.',given_table,' (',given_columns,')');
        PREPARE st FROM @sqlstmt;
        EXECUTE st;
        DEALLOCATE PREPARE st;
    -- ELSE
		-- debug info message
        -- SELECT CONCAT('Index ',given_index,' already exists on Table ',given_database,'.',given_table) CreateindexErrorMessage;   
    END IF;

END $$

DELIMITER ;

call createindex('FULLTEXT','sensmapserver','feeds','fulltext_index ','title,description,alias,name,ele');


drop procedure if exists `sensmapserver`.`CreateIndex`;

DELIMITER $$



-- -----------------------------
-- Create new foreign key
-- -----------------------------


DROP PROCEDURE IF EXISTS `sensmapserver`.`CreateFK` $$
CREATE PROCEDURE `sensmapserver`.`CreateFK`
(
    given_database VARCHAR(64),
    given_table    VARCHAR(64),
    given_fk       VARCHAR(64),
    given_index    VARCHAR(64),
    given_references_table VARCHAR(64),
    given_references_index VARCHAR(64),
    given_action VARCHAR(64),
    given_update_action VARCHAR(64)
)
BEGIN
	DECLARE FKisthere INTEGER;
	SET FOREIGN_KEY_CHECKS=0;
    SELECT COUNT(1) INTO FKisthere
		FROM information_schema.TABLE_CONSTRAINTS WHERE
		   CONSTRAINT_SCHEMA = given_database AND
		   TABLE_NAME        = given_table AND
		   CONSTRAINT_NAME   = given_fk AND
		   CONSTRAINT_TYPE   = 'FOREIGN KEY';
		   
	IF FKisthere = 0 THEN
        SET @sqlstmt = CONCAT('ALTER TABLE ',given_table,' ADD CONSTRAINT ',given_fk,' FOREIGN KEY (',given_index,') REFERENCES ',given_references_table,'(',given_references_index,')  ON DELETE ',given_action,' ON UPDATE ', given_update_action, ' ');
        PREPARE st FROM @sqlstmt;
        EXECUTE st;
        DEALLOCATE PREPARE st;
    -- ELSE
		-- debug info message
        -- SELECT CONCAT('FK ',given_fk,' already exists on Table ',given_database,'.',given_table) CreateindexErrorMessage;   
    END IF;
	SET FOREIGN_KEY_CHECKS=1;

END $$

DELIMITER ;
-- ALTER TABLE `zones_history` ADD FOREIGN KEY ( `zone_id` ) REFERENCES `sensmapserver`.`zones` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT ;

call createFK('sensmapserver','category','category_ibfk_1','parent','category','id', 'CASCADE', 'RESTRICT');
call createFK('sensmapserver','feed_category','feed_category_ibfk_1','category','category','id', 'CASCADE', 'RESTRICT');
call createFK('sensmapserver','feed_category','feed_category_ibfk_2','feed','feeds','id', 'CASCADE', 'RESTRICT');
call createFK('sensmapserver','position_history','position_history_ibfk_1','feed_reference','feeds','id', 'CASCADE', 'RESTRICT');
call createFK('sensmapserver','initialization_detail','initialization_detail_ibfk_1','initialization_reference','initialization','id', 'CASCADE', 'RESTRICT');
call createFK('sensmapserver','paths','paths_ibfk_1','building_reference','feeds','id', 'CASCADE', 'RESTRICT');
call createFK('sensmapserver','exits','exits_ibfk_1','plan_reference','plans','id', 'CASCADE', 'RESTRICT');
call createFK('sensmapserver','plans','plans_ibfk_1','building_reference','feeds','id', 'CASCADE', 'RESTRICT');
call createFK('sensmapserver','plans','plans_ibfk_2','model_reference','models','id','SET NULL', 'RESTRICT');
call createFK('sensmapserver','feeds','feeds_ibfk_1','model_reference','models','id','SET NULL', 'RESTRICT');
call createFK('sensmapserver','restriction_slaves','restriction_slaves_ibfk_1','master_reference','restriction_masters','id','CASCADE', 'RESTRICT');
call createFK('sensmapserver','restriction_masters','restriction_masters_ibfk_1','settings_reference','restriction_settings','id','SET NULL', 'RESTRICT');
call createFK('sensmapserver','zone_group','zone_group_ibfk_1','zone_group_reference','zones_groups','id','CASCADE', 'CASCADE');
call createFK('sensmapserver','zone_group','zone_ref_ibfk_1','zone_reference','zones','id','CASCADE', 'CASCADE');
call createFK('sensmapserver','zones','zones_ibfk_1','zone_settings_reference','zones_settings','id','SET NULL', 'CASCADE');
call createFK('sensmapserver','zones','zones_ibfk_2','out_zone_reference','zones','id','SET NULL', 'CASCADE');
call createFK('sensmapserver','zones','zones_ibfk_3','prein_zone_reference','zones','id','SET NULL', 'CASCADE');


drop procedure if exists `sensmapserver`.`createFK`;

-- -----------------------------
-- Drop old unnecessary tables
-- -----------------------------

DROP TABLE IF EXISTS `pma_bookmark`;
DROP TABLE IF EXISTS `pma_column_info`;
DROP TABLE IF EXISTS `pma_designer_coords`;
DROP TABLE IF EXISTS `pma_history`;
DROP TABLE IF EXISTS `pma_pdf_pages`;
DROP TABLE IF EXISTS `pma_recent`;
DROP TABLE IF EXISTS `pma_relation`;
DROP TABLE IF EXISTS `pma_table_coords`;
DROP TABLE IF EXISTS `pma_table_info`;
DROP TABLE IF EXISTS `pma_table_uiprefs`;
DROP TABLE IF EXISTS `pma_tracking`;
DROP TABLE IF EXISTS `pma_userconfig`;
	
	
-- --------------------------------------------------------------------
-- set larger group_concat in case of long plan names or multiple plans
-- --------------------------------------------------------------------

SET GLOBAL group_concat_max_len=6999;

-- --------------------------------------------------------------------
-- delete old unused mysql accounts
-- --------------------------------------------------------------------
DELETE FROM mysql.user WHERE User='sewio_admin_db';
