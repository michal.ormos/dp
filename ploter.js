var gnuplot = require('gnu-plot');

var gnuplot_s = require('gnuplot');

var plot = require('plotter').plot;
const fs = require('fs');

/**
 * util functions for plot results
 */

var PLOT = {};

module.exports = PLOT;

PLOT.init = function(val_gen_x, val_gen_y, val_ws_x, val_ws_y, fileName) {
  var dataset_gen = [];
  var dataset_ws = [];
  for(var i = 0; i < val_gen_x.length; i++) {
    var temp = [];
    temp[0] = val_gen_x[i];
    temp[1] = val_gen_y[i];
    dataset_gen.push(temp);
  }
  for(var i = 0; i < val_ws_x.length; i++) {
    var temp = [];
    temp[0] = val_ws_x[i];
    temp[1] = val_ws_y[i];
    dataset_ws.push(temp);
  }  
//   var data = dataset_gen,
//   out = fs.createWriteStream('out.png'),
//   plotter = gnuplot().set('term png');
  
// data.pipe(plotter).pipe(out);  

// gnuplot().plot([{
//       title:"Generator",
//       style:"linespoints",
//       data:dataset_gen
//     },{
//       title:"RTLS",
//       style:"linespoints",
//       data:dataset_ws
//     }])
    plot({
      data:		{ 'WS' : dataset_ws, 'GEN' : dataset_gen },
      filename:	fileName,
      style:		'linespoints',
      title:		'Example \'Title\', \\n runs onto multiple lines',
      xlabel:		'time',
      ylabel:		'length of string',
    });
}