"use strict"

/**
 * @file pac.js
 * Position accuracy calculator

 * @author ormos
 * integration
 * 
 * 
 */

 var PAC = {};
 module.exports = PAC;

 var i = 0;          // counter
 var category01 = 0;
 var category12 = 0;  
 var category23 = 0;
 var category34 = 0;
 var category45 = 0; 
 var category5 = 0;   
 var hist_data = []; // data for histogram
//  var dot_data = [];  // data for dots graph 

// computes distance between two positions 
PAC.dst = function(x, y, real_x, real_y) {
    return Math.sqrt(Math.pow(x - real_x, 2) + Math.pow(y - real_y, 2));
  }

    // copies data to dot_data and categorize data
PAC.histogram = function(data, real_x, real_y) {
    // dot_data.push(data); 
    var dst_value = PAC.dst(data.x, data.y, real_x, real_y) * 100;
    if (dst_value < 20) {
        category01++;
    }
    if (dst_value < 40 && dst_value >= 20) {
        category12++;
    }
    if (dst_value < 60 && dst_value >= 40) {
        category23++;
    }
    if (dst_value < 80 && dst_value >= 60) {
        category34++;
    }
    if (dst_value < 100 && dst_value >= 80) {
        category45++;
    }
    if (dst_value >= 100) {
        category5++;
    }
    i++;
    return;
}

PAC.main = function(DATA, real_x, real_y, real_mac) {
    var data;
    for (var tag in DATA) {
        if (DATA[tag].mac == real_mac) {
            data = DATA[tag];
            continue;
        }
    }
    // dot_data.push({x: real_x, y: real_y, real: "true"}); // makes difference between true position and tag reported postions

	// console.log("=============================1");
    // console.log(data);
    // console.log("=============================2");
    // console.log(real_x);
    // console.log("=============================3");
    // console.log(real_y);
	// console.log("=============================4");

    for (var item in data.data_object) {
        PAC.histogram(data.data_object[item], real_x, real_y);
    }
    // func_data.forEach(PAC.histogram(data, real_x, real_y));

    hist_data.push({ Category: "0-20 cm", Value: category01 });
    hist_data.push({ Category: "20-40 cm", Value: category12 });
    hist_data.push({ Category: "40-60 cm", Value: category23 });
    hist_data.push({ Category: "60-80 cm", Value: category34 });
    hist_data.push({ Category: "80-100 cm", Value: category45 });
    hist_data.push({ Category: "100+ cm", Value: category5 });

    return hist_data;
}
