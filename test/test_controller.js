"use strict"

/**
 * @file test_controller.js
 * 
 * Integration tests for RTLS system as Black box
 * @author ormos
 * 
 * Parts:
 * 0) Backup database before tests
 * 1) Generated payload tests
 * 2) A pcap
 * 3) B pcap
 * 4) C pcap
 * 5) C_static pcap
 * 6) Restore databa backape from 0)
 */

 /**
  * Libraries
  */
 // shell for executing shell commands
 var shell = require('shelljs');
 var request = require('supertest');
 var should = require('should');
 var assert = require('assert');
 var client = require('scp2');
 const addContext = require('mochawesome/addContext');
 var fs = require('fs');

 var node_ssh, ssh;
node_ssh = require('node-ssh')
ssh = new node_ssh()

/**
 * Files with json information needed
 */
var building = require('../Generated/building.json');
var anchors = require('../Generated/anchors_full.json');
var tag = require('../Generated/tag.json');
var zones = require('../Generated/50ZonesKancl210.json');
/**
 * Module for asking and gathering information from command line
 */
// var input_parser = require('../input_parser.js')

/**
 * Weboscket listner for RTLS system
 * and output position system calculate
 */
var websocketListener = require('../ws_listener.js');

/**
 * Module for playing pcap file, sending them to port 5000 of RTLS server
 */
var pcap_player = require('../pcap_p.js');

/**
 * Module for drawing results of pcap plays to svg and then html document
 */
var createSVG = require('../create_svg.js');

/**
 * Generated of anchors and tags
 * Which generate report to RTLS system
 * and output position for after comparision
 */
var reportGenerator = require('../ml_report_generator.js');

/**
 * Module for calculating Round Mean Square Error 
 * and Mean Absolute Percentage Error
 */
var RMSE = require('../rmse');
var MAPE = require('../mape');
var PAC = require('../pac');
/**
 * Module for using GnuPlot for purpose of ploting results
 */
// var PLOT = require('../ploter');

/**
 * Debug tool for unclosed socket and connections
 */
// var wtf = require('wtfnode');

var configIni = require('config.ini');
var conf = configIni.load('./config.ini');
/**
 * Variables and Constants
 */
var BUILDING_ID = 0;
var ANCHORS_ID = [];
var TAG_ID = 0;
var BLINK_ERR = conf.Generated.BLINK_ERROR;
var SYNC_ERR = conf.Generated.SYNC_ERROR;
var NUMBER_OF_TAGS = conf.Generated.NUMBER_OF_TAGS;
var SLOW_TAG = conf.Generated.SLOW_TAG;
var TAG_REFRESH_RATE = 300;
var gen_DATAx, gen_DATAy, gen_DATA_object, gen_DATA_string, ws_DATAx, ws_DATAy, ws_DATA;

var API_KEY = conf.SectionOne.API_KEY;
var API_KEY_STRING = '' + API_KEY;
const SERVER_IP = conf.SectionOne.IP_ADDRESS;
const SERVER_URL = "http://" + SERVER_IP + "/sensmapserver";
const SERVER_LOGIN = conf.SectionOne.USERNAME;
const SERVER_PASSWORD = conf.SectionOne.PASSWORD;
var PORTS = [];

console.log("===============================================================================");
console.log("Test suite");
console.log(new Date());
console.log("Server IP: " +  SERVER_IP + "");
(conf.SectionTwo.GENERATED_TEST ? console.log(" Generated TEST:  running") : true );
(conf.SectionTwo.ANTWERPEN_PCAP ? console.log("A PCAP:  running") : true );
(conf.SectionTwo.HASSLOCH_PCAP ? console.log("B: running") : true );
(conf.SectionTwo.TDK_PCAP ? console.log("TDK PCAP: running") : true );
(conf.SectionTwo.TDK_STATIC_PCAP ? console.log("TDK STATIC_PCAP: running") : true );
(conf.SectionTwo.REST_TEST ? console.log("REST TEST: running") : true );
(conf.SectionTwo.PAYLOAD_API_TEST ? console.log("PAYLOAD API TEST: running") : true );
console.log("===============================================================================");

// before('Get the APIKEY',function(done) {
//     parseInt(shell.exec('wc -l B/capture_file', {
//         silent: true
//     }).stdout.split(" ")[0]);
//     ssh.connect({
//         host: SERVER_IP,
//         username: SERVER_LOGIN,
//         password: SERVER_PASSWORD
//     })
//     .then(function() {
//         ssh.execCommand('grep API_KEY=* /home/rtlsserver/config.ini | cut -d "=" -f 2').then(function(result) {
//             API_KEY = result.stdout;
//             API_KEY_STRING = '' + API_KEY;   
//             ssh.dispose();
//             done();
//         });
//     });
// });


// before('Backup system before tests',function(done) {
//     ssh.connect({
//         host: SERVER_IP,
//         username: SERVER_LOGIN,
//         password: SERVER_PASSWORD
//     })
//     .then(function() {
//         ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S mysqldump -uroot -psensmap sensmapserver > /home/sewiortls/sensmapserver_test_backup.sql').then(function(result) {
//             console.log("here")
//             console.log(result);    
//             ssh.dispose();
//             done();
//         });
//         console.log("here2")                                                
//     });
// });

// before('Copy convertors',function(done) {
//     client.scp('convertor/', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/convertor', function(err) {
//         if (err) {
//             console.log(err);
//         } else {
//             console.log('Convertors transferred.');
//             done();
//         }
//     });
// });  
/*************************
 * Generated Payload Tests
 */
// (conf.SectionTwo.GENERATED_TEST ? describe : describe.skip)('Simulated Paypload Tests', function() {
//     var planScale = 104.51735850759;
//     var originX = 0;
//     var originY = 0;
//     var offsetX = originX / planScale;
//     var offsetY = originY / planScale;    

//     /** Permanently disabled
//      * 
//     describe('Get variables', function() {
//         it('Get variables', function(done) {
//             input_parser.main(function(data) {
//                 console.log("\n");
//                 SERVER_IP = SERVER_IP;//data[0];
//                 TAG_REFRESH_RATE = 200;//data[1];
//                 BLINK_ERR = 0;//data[2];
//                 SYNC_ERR = 0;//data[3];
//                 console.log([SERVER_IP, TAG_REFRESH_RATE, BLINK_ERR, SYNC_ERR]);
//                 done();
//             });
//         });         
//     });  
//     *  
//     */
//     describe('Initialization', function() {

//         it('Move ',function(done) {
//             shell.cp('Generated/config.ini','/home/rtlsserver/config.ini');
//             shell.cp('Generated/kancl.png','/var/www/html/sensmapserver/svgs/uploads/plans/');
//             shell.exec('service rtlsserver restart');
//             done();
//         });


//         // Create necessary building
//         it('Create new building', function(done) {
//             request(SERVER_URL)
//             .post('/api/buildings')
//             .set({
//                 'X-APIKey': "17254faec6a60f58458308763"
//             })
//             .send(building)
//             .end(function(err, res) {
//                 if (err) {
//                     throw err;
//                 }
//                 console.log("Buildind id is: " + res.body.id);
//                 BUILDING_ID = res.body.id;

//                 res.status.should.be.equal(200);
//                 done();
//             });
//         });    
     
//         // Create necessary anchors
//         anchors.forEach(function (anchor) {
//             it('Create new anchor', function(done) {
//                 request(SERVER_URL)
//                 .post('/api/anchors')
//                 .set({
//                     'X-APIKey': "17254faec6a60f58458308763"
//                 })
//                 .send(anchor)
//                 .end(function(err, res) {
//                     if (err) {
//                         throw err;
//                     }
//                     ANCHORS_ID.push(res.body.id);
//                     res.status.should.be.equal(200);
//                     done();
//                 });
//             });    
//         });   
//         it('Create the tag', function(done) {
//             request(SERVER_URL)
//             .post('/api/tags')
//             .set({
//                 'X-APIKey': "17254faec6a60f58458308763"
//             })
//             .send(tag)
//             .end(function(err, res) {
//                 if (err) {
//                     throw err;
//                 }
//                 console.log("tag id is: " + res.body.id);
//                 TAG_ID = res.body.id;
//                 res.status.should.be.equal(200);
//                 done();
//             });
//         });  
//         zones.forEach(function (zone) {
//             it('Create 50 zones', function(done) {
//                 request(SERVER_URL)
//                 .post('/api/buildings/'+BUILDING_ID+'/plans/kancl/zones')
//                 .set({
//                     'X-APIKey': "17254faec6a60f58458308763"
//                 })
//                 .send(zone)
//                 .end(function(err, res) {
//                     if (err) {
//                         throw err;
//                     }
//                     res.status.should.be.equal(200);
//                     done();
//                 });
//             });    
//         });              
//     });    

//     /**
//      * 
//      */
//     describe('Main test', function() {
//         // before('==PART 0 | Start Webocket',function(done) {
//         //     websocketListener.main(SERVER_IP, offsetX, offsetY, function() {
//         //         console.log("\n");
//         //         done();
//         //     });
//         // });

//         // before("Timeout for websocket to start", function(done) {
//         //     setTimeout(function(){ done(); }, 5000);
//         // });  

//         it('==PART 1 | Start Generating Reports',function(done) {
//             reportGenerator.main(SERVER_IP, BLINK_ERR, TAG_REFRESH_RATE, SYNC_ERR, NUMBER_OF_TAGS, SLOW_TAG, function(DATAx, DATAy, DATA_object, DATA_string) {
//                 gen_DATAx = DATAx;
//                 gen_DATAy = DATAy;
//                 gen_DATA_object = DATA_object;
//                 gen_DATA_string = DATA_string;
//                 done();
//             }); 
//         });
    
//         // it("Generate and Gather DATA from generator", function(done) {
//         //     console.log("[REPORT] Generated Data X: ", gen_DATAx.length);
//         //     console.log("[REPORT] Generated Data Y: ", gen_DATAy.length);
//         //     addContext(this, '[REPORT] Generated Data X: ' + gen_DATAx.length);
//         //     addContext(this, '[REPORT] Generated Data Y: ' + gen_DATAy.length);  
//         //     done();
//         // }); 

//         // it("Timeout for RTLS server to process all data", function(done) {
//         //     setTimeout(function(){ done(); }, 10000);
//         // }); 

//         // it("Gather DATA from websocket", function(done) {     
//         //     ws_DATAx = websocketListener.getDataX();
//         //     ws_DATAy = websocketListener.getDataY();
//         //     ws_DATA = websocketListener.getData();
//         //     ws_DATA.push({id:"99", mac:"0x999999999999", data_object:gen_DATA_object, data_string:gen_DATA_string})
//         //     websocketListener.closeWebsocket();
//         //     console.log("[REPORT] Gathered Data X: ", ws_DATAx.length);
//         //     console.log("[REPORT] Gathered Data Y: ", ws_DATAy.length);  
//         //     addContext(this, '[REPORT] Gathered Data X: ' + ws_DATAx.length);
//         //     addContext(this, '[REPORT] Gathered Data Y: ' + ws_DATAy.length);                      
//         //     done();
//         // });   

//         // it("Compare values and calculate RMSE error", function(done) {
//         //     console.log("RMSE of x", RMSE.init(gen_DATAx, ws_DATAx));
//         //     console.log("RMSE of y", RMSE.init(gen_DATAy, ws_DATAy));
//         //     addContext(this, 'RMSE of x: ' + RMSE.init(gen_DATAx, ws_DATAx));
//         //     addContext(this, 'RMSE of y: ' + RMSE.init(gen_DATAy, ws_DATAy));
//         //     done();
//         // });

//         // it("Compare values and calculate MAPE error", function(done) {
//         //     console.log("MAPE of x", MAPE.init(gen_DATAx, ws_DATAx));
//         //     console.log("MAPE of y", MAPE.init(gen_DATAy, ws_DATAy));
//         //     addContext(this, 'MAPE of x: ' + MAPE.init(gen_DATAx, ws_DATAx));
//         //     addContext(this, 'MAPE of y: ' + MAPE.init(gen_DATAy, ws_DATAy));            
//         //     done();
//         // });   

//         // it("Print graphs", function(done) {      
//         //     // PLOT.init(gen_DATAx, gen_DATAy, ws_DATAx, ws_DATAy, 'generator_output.png')
//         //     var actual_date = new Date().getTime();
//         //     createSVG.main(ws_DATA, ['0x999999999999', '0x060504030201'], 'kancl.png', planScale, actual_date);
//         //     addContext(this, SERVER_IP+':6210/kancl'+actual_date+'.html');               
//         //     websocketListener.resetData();
//         //     done();
//         // });    
        
//     });    

// /**
//  * 
//  */
//     describe('Cleaning', function() {
//         // Delete necessary building
//         it('Delete building', function(done) {
//             request(SERVER_URL)
//                 .delete('/api/buildings/' + BUILDING_ID)
//                 .set({
//                     'X-APIKey': "17254faec6a60f58458308763"
//                 })
//                 .end(function(err, res) {
//                     if (err) {
//                         throw err;
//                     }
//                     res.status.should.be.equal(200);
//                     done();
//                 });
//         });  

//         // Delete necessary anchors
//         it('Delete anchors', function(done) {
//             for (var id in ANCHORS_ID) {
//                 request(SERVER_URL)
//                     .delete('/api/anchors/' + ANCHORS_ID[id])
//                     .set({
//                         'X-APIKey': "17254faec6a60f58458308763"
//                     })
//                     .end(function(err, res) {
//                         if (err) {
//                             throw err;
//                         }
//                         res.status.should.be.equal(200);
//                     });
//             }
//             done();
//         });  
                
//         // it('Get tag', function(done) {
//         //     request(SERVER_URL)
//         //         .get('/api/tags')
//         //         .set({
//         //             'X-APIKey': '17254faec6a60f58458308763'
//         //         })
//         //         .end(function(err, res) {
//         //             res.status.should.be.equal(200);
//         //             TAG_ID = res.body.results[0].id;
//         //             done();
//         //     });
//         // });
//         it('Delete tag', function(done) {
//             request(SERVER_URL)
//                 .delete('/api/tags/'+TAG_ID)
//                 .set({
//                     'X-APIKey': "17254faec6a60f58458308763"
//                 })
//                 .end(function(err, res) {
//                     res.status.should.be.equal(200);
//                     done();
//             });            
//         });    
//     });          
// });


// /*************************
//  * CP A
//  * X 1.4039577836411046
//  * Y -1.6517150395780007
//  * Scale 8.981400107720903
//  * 0x00205F150CBC | 93ms | 5743
//  */
(conf.SectionTwo.A_PCAP ? describe : describe.skip)('Pcap A', function() {
    var planScale = 8.981400107720903;
    var originX = 1.4039577836411046;
    var originY = -1.6517150395780007;
    var offsetX = originX / planScale;
    var offsetY = originY / planScale;
    describe('Initialization', function() {
        it('Init pcap A', function(done) {
            shell.cp('A/sensmapserver_backup.sql','/home/rtlsserver/config.ini');
            shell.cp('A/config.ini','/home/rtlsserver/config.ini');
            shell.exec('service rtlsserver restart');
            shell.cp('-r', 'A/uploads/','/home/rtlsserver/config.ini');
            done();
        });

    });

    describe('Test', function() {
        // before('==PART 0 | Start Webocket',function(done) {
        //     websocketListener.main(SERVER_IP, offsetX, offsetY, function() {
        //         console.log("\n");
        //         done();
        //     });
        // });    

        // it("Timeout for websocket to start", function(done) {
        //     setTimeout(function(){ done(); }, 5000);
        // });     

        it('Run pcap A', function(done) {
            var file_length = parseInt(shell.exec('wc -l A/capture_file', {
                silent: true
            }).stdout.split(" ")[0]);
            pcap_player.main('A/capture_file', file_length, SERVER_IP, function() {
                done();
            })
        });  
        // it("Timeout for RTLS server to process all data", function(done) {
        //     setTimeout(function(){ done(); }, 10000);
        // }); 

        // it("Gather DATA from websocket. Unknow ideal limit for tag", function(done) {     
        //     ws_DATA = websocketListener.getData();
        //     websocketListener.closeWebsocket();
            
        //     addContext(this, '[REPORT] Gathered Data: ' + ws_DATA.length);          
        //     for (var data in ws_DATA) {
        //         if (ws_DATA[data].mac != undefined) {
        //             addContext(this, '[REPORT] Tag ID:' + ws_DATA[data].id + ' | Mac:' + ws_DATA[data].mac + ' | Gathered positions:' + ws_DATA[data].data_object.length);
        //         }
        //         // if (ws_DATA[data].mac == '0x00205F150CBC') {
        //         //     ws_DATA[data].data_object.length.should.be.above(4300);
        //         // }
        //     }    
        //     done();
        // });  
        // it("Print graphs", function(done) { 
        //     var actual_date = new Date().getTime(); 
        //     var printData = {
        //         scale: 8.4326197727071,
        //         plan_width: 630,
        //         plan_height: 660,
        //         origin_x: 31.326397919376,
        //         origin_y: 26.605981794538,
        //         plan_name: 'a.png'
        //     };
        //     createSVG.main(ws_DATA, 0, actual_date, printData, SERVER_IP, SERVER_LOGIN, SERVER_PASSWORD);
        //     addContext(this, SERVER_IP+':6210/'+actual_date+'.html');     
        //     websocketListener.resetData();
        //     done();
        // });           
    });       
});   

// /*************************
//  * CP B
//  * X 125.83933649289114
//  * Y 242.07898894154934
//  * Scale 27.32413075200172
//  * 0x00205F1510F8 | 1179ms | 204
//  * 0x00205FE1332A | 1184ms | 202
//  */    
// (conf.SectionTwo.B_PCAP ? describe : describe.skip)('Pcap B', function() {
//     var planScale = 27.32413075200172;
//     var originX = 125.83933649289114;
//     var originY = 242.07898894154934;
//     var offsetX = originX / planScale;
//     var offsetY = originY / planScale;
//     describe('Initialization', function() {
//         // Copy config.ini with right settings
//         it('Copy sensmapserver_backup.sql',function(done) {
//             client.scp('B/sensmapserver_backup.sql', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('DB transferred.');
//                     done();
//                 }
//             });
//         });
            
//         it('Set sensmapserver',function(done) {
//             ssh.connect({
//                 host: SERVER_IP,
//                 username: SERVER_LOGIN,
//                 password: SERVER_PASSWORD
//             })

//             .then(function() {
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs /builds/michal.ormos/dp/restart_db.js').then(function(result) {
//                     console.log(result);  
//                     ssh.dispose();  
//                     done();
//                 });                                                
//             });
//         });      

//         it('Copy plans',function(done) {
//             client.scp('B/uploads/', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/uploads/', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('plans transferred.');
//                     done();
//                 }
//             });
//         });   
//         it('Set plans',function(done) {
//             ssh.connect({
//                 username: SERVER_LOGIN,
//                 host: SERVER_IP,
//                 password: SERVER_PASSWORD
//             })
//             .then(function() {              
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S cp -r uploads/ /var/www/html/sensmapserver/svgs/').then(function(result) {
//                     console.log(result);
//                     ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/plan_converter/convert').then(function(result) {
//                         console.log(result); 
//                         ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/plan_converter/convert_devices').then(function(result) {
//                             console.log(result);
//                             ssh.dispose();                                             
//                             done();
//                         });    
//                     });                                              
//                 });                                              
//             });
//         });  

//         it('Copy config.ini',function(done) {
//             client.scp('B/config.ini', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/config.ini', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('configs transferred.');
//                     done();
//                 }
//             });
//         });      

//         it('Set config',function(done) {
//             ssh.connect({
//                 username: SERVER_LOGIN,
//                 host: SERVER_IP,
//                 password: SERVER_PASSWORD
//             })
//             .then(function() {              
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/rtlsconfig/convert.js /home/sewiortls/config.ini /home/sewiortls/convertor/rtlsconfig/config.ini > /home/sewiortls/config2.ini').then(function(result) {
//                     console.log(result);
//                     ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/UPLOAD_TO_SENSMAPSERVER=Disabled/UPLOAD_TO_SENSMAPSERVER=REST/g" /home/sewiortls/config2.ini').then(function(result) {
//                         console.log(result);      
//                         ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/API_KEY=.*/API_KEY='+API_KEY+'/g" /home/sewiortls/config2.ini').then(function(result) {
//                             console.log(result);                                        
//                             ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S cp /home/sewiortls/config2.ini /home/rtlsserver/config.ini').then(function(result) {
//                                 console.log(result);                                             
//                                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S service rtlsserver restart').then(function(result) {
//                                     console.log(result);
//                                     ssh.dispose();
//                                     done();
//                                 });
//                             });                     
//                         });                     
//                     });
//                 });                                              
//             });
//         });  
//         it("RTLS Server restart", function(done) {
//             setTimeout(function(){ done(); }, 10000);
//         });          
//     });

//     describe('Test', function() {
//         before('==PART 0 | Start Webocket',function(done) {
//             websocketListener.main(SERVER_IP, offsetX, offsetY, function() {
//                 console.log("\n");
//                 done();
//             });
//         });    

//         it("Timeout for websocket to start", function(done) {
//             setTimeout(function(){ done(); }, 5000);
//         });     

//         it('Run pcap B', function(done) {
//             var file_length = parseInt(shell.exec('wc -l B/capture_file', {
//                 silent: true
//             }).stdout.split(" ")[0]);
//             pcap_player.main('B/capture_file', file_length, SERVER_IP, function() {
//                 done();
//             })
//         });  
//         it("Timeout for RTLS server to process all data", function(done) {
//             setTimeout(function(){ done(); }, 5000);
//         }); 
//         it("Gather DATA from websocket. 0x00205F1510F8 and 0x00205FE1332A should be more than 200", function(done) {     
//             ws_DATA = websocketListener.getData();
//             websocketListener.closeWebsocket();

//             addContext(this, '[REPORT] Gathered Data: ' + ws_DATA.length);          
//             for (var data in ws_DATA) {
//                 if (ws_DATA[data].mac != undefined) {
//                     addContext(this, '[REPORT] Tag ID:' + ws_DATA[data].id + ' | Mac:' + ws_DATA[data].mac + ' | Gathered positions:' + ws_DATA[data].data_object.length);
//                 }
//                 if (ws_DATA[data].mac == '0x00205F1510F8') {
//                     ws_DATA[data].data_object.length.should.be.above(200);
//                 }  
//             }
//             done();
//         });  
//         it("Print graphs", function(done) { 
//             var actual_date = new Date().getTime(); 
//             var printData = {
//                 scale: 27.32413075200172,
//                 plan_width: 1500,
//                 plan_height: 1321,
//                 origin_x: 25.83933649289114,
//                 origin_y: 242.07898894154934,
//                 plan_name: 'b.png'
//             };
//             createSVG.main(ws_DATA, 0, actual_date, printData, SERVER_IP, SERVER_LOGIN, SERVER_PASSWORD);
//             addContext(this, SERVER_IP+':6210/'+actual_date+'.html');     
//             websocketListener.resetData();
//             done();
//         });             
//     });       
// });
    
// /*************************
//  * CP C
//  * X - 407.80487804878
//  * Y - 691.21951219512
//  * Scale 55.491435516047
//  * ?
//  * /sensmapserver/svgs/uploads/plans/KgQhahHJjJaZyrYO8smDGDbKnSEc5rDeG9peXMZg.png
//  */
// (conf.SectionTwo.C_PCAP ? describe : describe.skip)('Pcap C', function() {
//     var planScale = 55.491435516047;
//     var originX = 407.80487804878;
//     var originY = 691.21951219512;
//     var offsetX = originX / planScale;
//     var offsetY = originY / planScale;     
//     describe('Initialization', function() {
//         // Copy config.ini with right settings
//         it('Copy sensmapserver_backup.sql',function(done) {
//             client.scp('C/sensmapserver_backup.sql', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('File transferred.');
//                     done();
//                 }
//             });
//         });
        
//         it('Set sensmapserver',function(done) {
//             ssh.connect({
//                 host: SERVER_IP,
//                 username: SERVER_LOGIN,
//                 password: SERVER_PASSWORD
//             })

//             .then(function() {
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs /builds/michal.ormos/dp/restart_db.js').then(function(result) {
//                     console.log(result);   
//                     ssh.dispose();
//                     done();
//                 });                                                
//             });
//         });      

//         it('Copy plans',function(done) {
//             client.scp('C/uploads/', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/uploads/', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('Plans transferred.');
//                     done();
//                 }
//             });
//         });   
//         it('Set plans',function(done) {
//             ssh.connect({
//                 username: SERVER_LOGIN,
//                 host: SERVER_IP,
//                 password: SERVER_PASSWORD
//             })
//             .then(function() {              
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S cp -r uploads/ /var/www/html/sensmapserver/svgs/').then(function(result) {
//                     console.log(result);
//                     ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/plan_converter/convert').then(function(result) {
//                         console.log(result); 
//                         ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/plan_converter/convert_devices').then(function(result) {
//                             console.log(result);
//                             ssh.dispose();
//                             done();
//                         });    
//                     });                                              
//                 });                                              
//             });
//         });  

//         it('Copy config.ini',function(done) {
//             client.scp('C/config.ini', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/config.ini', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('config transferred.');
//                     done();
//                 }
//             });
//         });      

//         it('Set config',function(done) {
//             ssh.connect({
//                 username: SERVER_LOGIN,
//                 host: SERVER_IP,
//                 password: SERVER_PASSWORD
//             })
//             .then(function() {              
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/rtlsconfig/convert.js /home/sewiortls/config.ini /home/sewiortls/convertor/rtlsconfig/config.ini > /home/sewiortls/config2.ini').then(function(result) {
//                     console.log(result);
//                     ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/UPLOAD_TO_SENSMAPSERVER=Disabled/UPLOAD_TO_SENSMAPSERVER=REST/g" /home/sewiortls/config2.ini').then(function(result) {
//                         console.log(result);  
//                         ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/API_KEY=.*/API_KEY='+API_KEY+'/g" /home/sewiortls/config2.ini').then(function(result) {
//                             console.log(result);                                           
//                             ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S cp /home/sewiortls/config2.ini /home/rtlsserver/config.ini').then(function(result) {
//                                 console.log(result);                                             
//                                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S service rtlsserver restart').then(function(result) {
//                                     console.log(result);
//                                     ssh.dispose();
//                                     done();
//                                 });
//                             });        
//                         });             
//                     });
//                 });                                              
//             });
//         });  
//         it("RTLS Server restart", function(done) {
//             setTimeout(function(){ done(); }, 10000);
//         });          
//     });

//     describe('Test', function() {
//         before('==PART 0 | Start Webocket',function(done) {
//             websocketListener.main(SERVER_IP, offsetX, offsetY, function() {
//                 console.log("\n");
//                 done();
//             });
//         });    

//         it("Timeout for websocket to start", function(done) {
//             setTimeout(function(){ done(); }, 5000);
//         });     

//         it('Run pcap C', function(done) {
//             var file_length = parseInt(shell.exec('wc -l C/capture_file', {
//                 silent: true
//             }).stdout.split(" ")[0]);
//             pcap_player.main('C/capture_file', file_length, SERVER_IP, function() {
//                 done();
//             })
//         });  

//         it("Timeout for RTLS server to process all data", function(done) {
//             setTimeout(function(){ done(); }, 5000);
//         }); 

//         it("Gather DATA from websocket. Tag TODO should be more than TODO", function(done) {     
//             ws_DATA = websocketListener.getData();
//             websocketListener.closeWebsocket();

//             addContext(this, '[REPORT] Gathered Data: ' + ws_DATA.length); 
//             for (var data in ws_DATA) {
//                 if (ws_DATA[data].mac != undefined) {
//                     addContext(this, '[REPORT] Tag ID:' + ws_DATA[data].id + ' | Mac:' + ws_DATA[data].mac + ' | Gathered positions:' + ws_DATA[data].data_object.length);
//                 }
//                 // if (ws_DATA[data].mac == '0x00205FE84957') {
//                 //     ws_DATA[data].data_object.length.should.be.above(600);
//                 // }  
//             }
//             done();
//         });        
//         it("Print graphs", function(done) { 
//             var actual_date = new Date().getTime(); 
//             var printData = {
//                 scale: 55.491435516047,
//                 plan_width: 6000,
//                 plan_height: 5000,
//                 origin_x: 407.80487804878,
//                 origin_y: 691.21951219512,
//                 plan_name: 'c.png'
//             };
//             createSVG.main(ws_DATA, 0, actual_date, printData, SERVER_IP, SERVER_LOGIN, SERVER_PASSWORD);
//             addContext(this, SERVER_IP+':6210/'+actual_date+'.html');     
//             websocketListener.resetData();
//             done();
//         }); 
//     });       
// });  

// /*************************
//  * CP C_static 
//  * X - 407.80487804878
//  * Y - 691.21951219512
//  * Scale 55.491435516047
//  * ?
//  * /sensmapserver/svgs/uploads/plans/KgQhahHJjJaZyrYO8smDGDbKnSEc5rDeG9peXMZg.png
//  */
// (conf.SectionTwo.C_STATIC_PCAP ? describe : describe.skip)('Pcap Static C_static', function() {
//     var planScale = 55.491435516047;
//     var originX = 407.80487804878;
//     var originY = 691.21951219512;
//     var offsetX = originX / planScale;
//     var offsetY = originY / planScale;     
//     describe('Initialization', function() {
//         // Copy config.ini with right settings
//         it('Copy sensmapserver_backup.sql',function(done) {
//             client.scp('C_static/sensmapserver_backup.sql', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('File transferred.');
//                     done();
//                 }
//             });
//         });
        
//         it('Set sensmapserver',function(done) {
//             ssh.connect({
//                 host: SERVER_IP,
//                 username: SERVER_LOGIN,
//                 password: SERVER_PASSWORD
//             })

//             .then(function() {
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs /builds/michal.ormos/dp/restart_db.js').then(function(result) {
//                     console.log(result);   
//                     ssh.dispose();
//                     done();
//                 });                                                
//             });
//         });      

//         it('Copy plans',function(done) {
//             client.scp('C_static/uploads/', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/uploads/', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('Plans transferred.');
//                     done();
//                 }
//             });
//         });   
//         it('Set plans',function(done) {
//             ssh.connect({
//                 username: SERVER_LOGIN,
//                 host: SERVER_IP,
//                 password: SERVER_PASSWORD
//             })
//             .then(function() {              
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S cp -r uploads/ /var/www/html/sensmapserver/svgs/').then(function(result) {
//                     console.log(result);
//                     ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/plan_converter/convert').then(function(result) {
//                         console.log(result); 
//                         ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/plan_converter/convert_devices').then(function(result) {
//                             console.log(result);
//                             ssh.dispose();
//                             done();
//                         });    
//                     });                                              
//                 });                                              
//             });
//         });  

//         it('Copy config.ini',function(done) {
//             client.scp('C_static/config.ini', 'sewiortls:sensmap@'+SERVER_IP+':/home/sewiortls/config.ini', function(err) {
//                 if (err) {
//                     console.log(err);
//                 } else {
//                     console.log('config transferred.');
//                     done();
//                 }
//             });
//         });      

//         it('Set config',function(done) {
//             ssh.connect({
//                 username: SERVER_LOGIN,
//                 host: SERVER_IP,
//                 password: SERVER_PASSWORD
//             })
//             .then(function() {              
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs convertor/rtlsconfig/convert.js /home/sewiortls/config.ini /home/sewiortls/convertor/rtlsconfig/config.ini > /home/sewiortls/config2.ini').then(function(result) {
//                     console.log(result);
//                     ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/UPLOAD_TO_SENSMAPSERVER=Disabled/UPLOAD_TO_SENSMAPSERVER=REST/g" /home/sewiortls/config2.ini').then(function(result) {
//                         console.log(result);  
//                         ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/API_KEY=.*/API_KEY='+API_KEY+'/g" /home/sewiortls/config2.ini').then(function(result) {
//                             console.log(result);                                           
//                             ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S cp /home/sewiortls/config2.ini /home/rtlsserver/config.ini').then(function(result) {
//                                 console.log(result);                                             
//                                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S service rtlsserver restart').then(function(result) {
//                                     console.log(result);
//                                     ssh.dispose();
//                                     done();
//                                 });
//                             });        
//                         });             
//                     });
//                 });                                              
//             });
//         });  
//         it("RTLS Server restart", function(done) {
//             setTimeout(function(){ done(); }, 10000);
//         });          
//     });

//     describe('Test', function() {
//         before('==PART 0 | Start Webocket',function(done) {
//             websocketListener.main(SERVER_IP, offsetX, offsetY, function() {
//                 console.log("\n");
//                 done();
//             });
//         });    

//         it("Timeout for websocket to start", function(done) {
//             setTimeout(function(){ done(); }, 5000);
//         });     

//         it('Run pcap C_static', function(done) {
//             var file_length = parseInt(shell.exec('wc -l C_static/capture_file', {
//                 silent: true
//             }).stdout.split(" ")[0]);
//             pcap_player.main('C_static/capture_file', file_length, SERVER_IP, function() {
//                 done();
//             })
//         });  

//         it("Timeout for RTLS server to process all data", function(done) {
//             setTimeout(function(){ done(); }, 5000);
//         }); 

//         it("Gather DATA from websocket. Tag TODO should be more than TODO", function(done) {     
//             ws_DATA = websocketListener.getData();
//             websocketListener.closeWebsocket();

//             addContext(this, '[REPORT] Gathered Data: ' + ws_DATA.length); 
//             for (var data in ws_DATA) {
//                 if (ws_DATA[data].mac != undefined) {
//                     addContext(this, '[REPORT] Tag ID:' + ws_DATA[data].id + ' | Mac:' + ws_DATA[data].mac + ' | Gathered positions:' + ws_DATA[data].data_object.length);
//                 }
//                 // if (ws_DATA[data].mac == '0x00205FE84957') {
//                 //     ws_DATA[data].data_object.length.should.be.above(600);
//                 // }  
//             }
//             done();
//         });  
//         it("Gather information about port drops", function(done) {
//             var drops, processedDrops;
//             ssh.connect({
//                 username: SERVER_LOGIN,
//                 host: SERVER_IP,
//                 password: SERVER_PASSWORD
//             })
//             .then(function() {
//                 ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S bash /var/www/html/studio/app/Http/Controllers/modules/shell_files/ports.sh').then(function(result) {
//                     drops = result.stdout;
//                     console.log(result);
//                     PORTS = JSON.parse(drops);
//                     console.log(JSON.parse(drops));
//                     console.log(processedDrops);
//                     ssh.dispose();
//                     done();
//                 });
//             });
//         });
//         it("Display information about port drops", function(done) {
//             addContext(this, 'Result Ports is: ' + PORTS);                   
//             for (var item in PORTS) {
//                 addContext(this, PORTS[item].port + ": " + PORTS[item].drops);
//             }
//             done();
//         });	    
//         it("Recalculate positions precision", function(done) {   
//             // console.log("PAC is", PAC.main(ws_DATA, 27.10, 43.93, "0x22025CE3FC42"));
//             var PACresults = PAC.main(ws_DATA, 27.10, 43.93, "0x22025CE3FC42");
//             console.log(PACresults);
//             addContext(this, 'Static postition precision for 0x22025CE3FC42 is: \n' + PACresults[0].Category + ": " + PACresults[0].Value + 
//             "\n" + PACresults[1].Category + ": " + PACresults[1].Value + 
//             "\n" + PACresults[2].Category + ": " + PACresults[2].Value + 
//             "\n" + PACresults[3].Category + ": " + PACresults[3].Value + 
//             "\n" + PACresults[4].Category + ": " + PACresults[4].Value + 
//             "\n" + PACresults[5].Category + ": " + PACresults[5].Value);
//             addContext(this, 'Result Ports is: ' + PORTS);                   
//             done();
//         });         
//         it("Print graphs", function(done) { 
//             var actual_date = new Date().getTime(); 
//             var printData = {
//                 scale: 55.491435516047,
//                 plan_width: 6000,
//                 plan_height: 5000,
//                 origin_x: 407.80487804878,
//                 origin_y: 691.21951219512,
//                 plan_name: 'c_static.png'
//             };
//             createSVG.main(ws_DATA, 0, actual_date, printData, SERVER_IP, SERVER_LOGIN, SERVER_PASSWORD);
//             addContext(this, SERVER_IP+':6210/'+actual_date+'.html');     
//             websocketListener.resetData();
//             done();
//         });                        
//     });       
// });  

after('Stop dgram', function(done) {
    pcap_player.closeClient();
    done();
});

// after('Restore DB to state before test',function(done) {
//         ssh.connect({
//             host: SERVER_IP,
//             username: SERVER_LOGIN,
//             password: SERVER_PASSWORD
//         })

//         .then(function() {
//             ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs /builds/michal.ormos/dp/restore_db.js').then(function(result) {
//                 console.log(result);  
//                 ssh.dispose();  
//                 done();
//             });                                                
//         });
// });     

// after('Restart RTLS server',function(done) {
//         ssh.connect({
//             username: SERVER_LOGIN,
//             host: SERVER_IP,
//             password: SERVER_PASSWORD
//         })
//         .then(function() {                        
//             ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S service rtlsserver restart').then(function(result) {
//                 console.log(result);
//                 ssh.dispose();
//                 // wtf.dump()
//                 done();
//             });                                              
//         });
//     });   

