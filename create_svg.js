var node_ssh, ssh;
node_ssh = require('node-ssh')
ssh = new node_ssh();
var fs = require('fs')

module.exports.main = function main(DATA, tagsToDraw, actual_date, printData, SERVER_IP, SERVER_LOGIN, SERVER_PASSWORD) {
  fs.writeFileSync('/builds/michal.ormos/dp/mochawesome-report/'+actual_date+".json", JSON.stringify(DATA, null, 2) , 'utf-8');
  ssh.connect({
      username: SERVER_LOGIN,
      host: SERVER_IP,
      password: SERVER_PASSWORD
  })
  .then(function() {      
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S cp /builds/michal.ormos/dp/mochawesome-report/mocha_template.html /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.html').then(function(result) {
          console.log(result);
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/FILL_INPUT_DATA.json/'+ actual_date +'.json/g" /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.html').then(function(result) {
          console.log(result);
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/FILL_INPUT_PLAN.png/'+printData.plan_name+'/g" /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.html').then(function(result) {
          console.log(result);
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/FILL_PLAN_WIDTH/'+printData.plan_width+'/g" /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.html').then(function(result) {
          console.log(result);
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/FILL_PLAN_HEIGHT/'+printData.plan_height+'/g" /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.html').then(function(result) {
          console.log(result);
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/FILL_PLAN_SCALE/'+printData.scale+'/g" /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.html').then(function(result) {
          console.log(result);
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/FILL_OFFSET_X/'+printData.origin_x+'/g" /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.html').then(function(result) {
          console.log(result);
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "s/FILL_OFFSET_Y/'+printData.origin_y+'/g" /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.html').then(function(result) {
          console.log(result);
      ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S sed -i "1iconst data = " /builds/michal.ormos/dp/mochawesome-report/'+actual_date+'.json').then(function(result) {
          console.log(actual_date);
          // createSVG.main(ws_DATA, 0, 'TDK/tdk.png', planScale, actual_date, 'tdk.png');
          ssh.dispose();                      
      });
    });
  });
});
});
});
});
});
});                                        
  });
}