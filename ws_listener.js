"use strict"

/**
 * @file ws_listener.js
 * 
 * Listener for RTLS Integration Tests
 * 
 * @author ormos
 * integration to mocha
 * 
 */

var websocket = require('websocket').w3cwebsocket;
var gnuplot = require('gnu-plot');

var API_KEY = "17254faec6a60f58458308763";
var method = 'subscribe';

var ws

/* shared variables to mocha */
var DATAx = [];
var DATAy = [];

var x_old, y_old;
var offsetX, offsetY;

var ARRAY_OF_IDs = [];

exports.getDataX = function() {
    return DATAx;
}

exports.getDataY = function() {
    return DATAy;
}

exports.closeWebsocket = function() {
    ws.close();
}

// exports.getDataId = function() {
//     return DATAid;
// }

// exports.getData = function() {
//     return DATA;
// }

// exports.getDataMac = function() {
//     return DATAmac;
// }

// exports.getDataString = function() {
//     return DATAstring;
// }
exports.getData = function() {
    return ARRAY_OF_IDs;
}

exports.resetData = function() {
    DATAx = [];
    DATAy = [];
    ARRAY_OF_IDs = [];
}

function start() {
    if (ws.readyState === ws.OPEN) {
        ws.send('{"headers":{"X-ApiKey":"' + API_KEY + '"},"method":"' + method + '", "resource":"/feeds/"}');
        console.log("[Socket ready]");
    } else {
        console.log("ERROR: sendFeedWebSocketMessage: Cannot subscribe tag from websocket %ws.readyState:%ws.OPEN" + ws.readyState + ":" + ws.OPEN);
    };
}

// hasOwnProperty
function onMessage(message) {
    var containsId = false;
    var id_recorded = 0;
    var x,y;
    if (JSON.parse(message.data).body.type != 'building') {
        if (JSON.parse(message.data).body.datastreams[1]) {
            for (var id in ARRAY_OF_IDs) {
                if (ARRAY_OF_IDs[id].id == parseFloat(JSON.parse(message.data).body.id)) {
                    id_recorded = id;    
                    containsId = true;            
                }
            }
            if (containsId == false) {   
                ARRAY_OF_IDs.push({id:parseFloat(JSON.parse(message.data).body.id), mac:JSON.parse(message.data).body.address, data_object:[], data_string:""})
                id_recorded = ARRAY_OF_IDs.length-1;
            }

            for ( var stream in JSON.parse(message.data).body.datastreams ) {
                if (JSON.parse(message.data).body.datastreams[stream]['id'] == "posX") {
                    x = parseFloat(JSON.parse(message.data).body.datastreams[stream]['current_value']);
                } else if (JSON.parse(message.data).body.datastreams[stream]['id'] == "posY") {
                    y = parseFloat(JSON.parse(message.data).body.datastreams[stream]['current_value']);
                }                    
            }
            if (typeof(x) == 'undefined') {
                x = x_old;
            }
            if (typeof(y) == 'undefined') {
                y = y_old;
            }
            x = x + offsetX;
            y = y + offsetY;
            ARRAY_OF_IDs[id_recorded].data_object.push({x: x, y: y});
            DATAx.push(x);
            DATAy.push(y);            
            var pre_string = "";
            pre_string = pre_string.concat(x);
            pre_string = pre_string.concat(",");
            pre_string = pre_string.concat(y);
            pre_string = pre_string.concat(" ");                  
            ARRAY_OF_IDs[id_recorded].data_string = ARRAY_OF_IDs[id_recorded].data_string.concat(pre_string);              
            x_old = x;
            y_old = y;      
            containsId = false;
            id_recorded = 0;             
        }
        //     var id = parseFloat(JSON.parse(message.data).body.id);
        //     var title = parseFloat(JSON.parse(message.data).body.title);
        //     var x;
        //     var y;
        //     for ( var stream in JSON.parse(message.data).body.datastreams ) {
        //         if (JSON.parse(message.data).body.datastreams[stream]['id'] == "posX") {
        //             x = parseFloat(JSON.parse(message.data).body.datastreams[stream]['current_value']);
        //         } else if (JSON.parse(message.data).body.datastreams[stream]['id'] == "posY") {
        //             y = parseFloat(JSON.parse(message.data).body.datastreams[stream]['current_value']);
        //         }                    
        //     }
        //     if (typeof(x) == 'undefined') {
        //         x = x_old;
        //     }
        //     if (typeof(y) == 'undefined') {
        //         y = y_old;
        //     }
        //     DATAid.push(id);
        //     DATAmac.push(title);            
        //     DATAx.push(x);
        //     DATAy.push(y);
        //     DATA.push({x: x, y: y});
        //     DATAstring = DATAstring.concat(x);
        //     DATAstring = DATAstring.concat(",");
        //     DATAstring = DATAstring.concat(y);
        //     DATAstring = DATAstring.concat(" ");            
        //     x_old = x;
        //     y_old = y;
        // }
    }
}

function onError(evt) {
    console.log(evt.data);
} 
// var server_ip = "172.16.18.177";
module.exports.main = function main(server_ip, offset_x, offset_y, callback) {
    ws = new websocket('ws://' + server_ip +':8080');
    offsetX = offset_x;
    offsetY = offset_y;

    ws.onopen = function(evt) {
        onOpen(evt)
    };
    
    ws.onclose = function(evt) {
        onClose(evt)
    };
    
    ws.onmessage = function(message) {
        onMessage(message);
    };
    
    ws.onerror = function(evt) {
        onError(evt)
    };   
    
    function onOpen(evt) {
        console.log("CONNECTED to Websocket");
    }
    
    function onClose(evt) {
        console.log("DISCONNECTED from Websocket");
    }    
    console.log("================ STARTING WEBSOCKET ================");
    setTimeout(function() {
       start();
       callback(); 
    }
    , 1000);
}

// setTimeout(function() {
//     start();
// } , 1000);