var client = require('dgram').createSocket('udp4');

var path = require('path');
var fs = require('fs');
var rl = require('readline');

module.exports.closeClient = function closeClient() {
    client.close();
}

module.exports.main = function main(pcapFileName, pcapFileLength, ipAddress, callback) {
    var lineInterface = rl.createInterface({
        input: fs.createReadStream(pcapFileName)
    });

    var delaySum = 0;
    var currentLine = 0;

    function sendSocket(message) {
        client.send(message, 0, message.length, 5000, ipAddress, function(err, bytes) {
            if(err) {
                console.log("error here");
                console.log(err);
                console.log(bytes);
                console.log(currentLine);
                console.log(message);
            }
        })
        currentLine++;

        if(currentLine == pcapFileLength) {
            // client.close();
            callback();
        } else if (currentLine % (Math.round(pcapFileLength/10)) == 0){
            console.log(Math.round(currentLine/pcapFileLength*100) + "%");
        }
        // } else if (currentLine == Math.round(pcapFileLength/10)) {
        //     console.log("10%")
        // } else if (currentLine == Math.round(pcapFileLength/2)) {
        //     console.log("50%")
        // } else if (currentLine == Math.round(pcapFileLength*(4/3))) {
        //     console.log("75%")
        // }
    };

    lineInterface.on('line', function (line) {
        var parts = line.replace(/['"]+/g, '').split(/\t/);

        var delay = parseFloat(parts[0]) >= 0 ? parseFloat(parts[0]) : 0;

        var newMessage = parts[1];

        var message = new Buffer(newMessage, 'hex');

        delaySum+=delay;

        setTimeout(sendSocket, delaySum*1000, message);        
    });
}