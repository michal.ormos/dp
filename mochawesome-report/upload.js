var CONFIGS = [];
var ARCHIVES = [];
var PAN_ZOOM = undefined;
var LOCKED = false;

var socket = io.connect('http://' + window.location.hostname + ':8210');

$('#sensmap-link').attr('href', 'http://' + window.location.hostname + '/sensmap');
$('#manager-link').attr('href', 'http://' + window.location.hostname + '/rtlsmanager');
$('#skip-button').click(() => {
  $.ajax({
    url: '/skip',
    type: 'POST',
    data: {},
    success: function(data){
    }
  });
});

$('#config-input').on('change', function() {
  var label = this.nextElementSibling;
  var files = $(this).get(0).files;

  if (files.length < 1) {
    return;
  }

  $("#config-success").fadeOut(400);

  $(label).animate({
    'opacity': 0
  }, 400, function() {
    $(this).html(files.length === 1 ? files[0].name : files.length + " Files Selected").animate({
      'opacity': 1
    }, 400, function() {
      $("#config-success").fadeIn(400);
    });
  });

  $("#config-current").prop('checked', false);
  $("#config-archive").prop('checked', false);

  CONFIGS = [];

  for (var i = 0; i < files.length; i++) {
    CONFIGS.push(files[i]);
  }  
});

$('#config-current').on('change', function() {
  $("#config-archive").prop('checked', false);
  
  var input = $('#config-input').get(0);
  var label = input.nextElementSibling;

  input.value = "";

  if (label.innerHTML !== "CHOOSE OWN") {
    $("#config-success").fadeOut(400, function() {
      $(label).animate({
        'opacity': 0
      }, 400, function() {
        $(this).html("CHOOSE OWN").animate({
          'opacity': 1
        }, 400);
      });
    });
  }

  CONFIGS = [];
});

$('#config-archive').on('change', function() {
  $("#config-current").prop('checked', false);

  var input = $('#config-input').get(0);
  var label = input.nextElementSibling;

  input.value = "";

  if (label.innerHTML !== "CHOOSE OWN") {
    $("#config-success").fadeOut(400, function() {
      $(label).animate({
        'opacity': 0
      }, 400, function() {
        $(this).html("CHOOSE OWN").animate({
          'opacity': 1
        }, 400);
      });
    });
  }

  CONFIGS = [];
});

$('#method-udp').on('change', function() {
  $("#method-socket").prop('checked', false);
  $("#method-none").prop('checked', false);
});

$('#method-socket').on('change', function() {
  $("#method-udp").prop('checked', false);
  $("#method-none").prop('checked', false);
});

$('#method-none').on('change', function() {
  $("#method-udp").prop('checked', false);
  $("#method-socket").prop('checked', false);
});

$('#archive-input').on('change', function() {
  var files = $(this).get(0).files;

  if (files.length < 1 || LOCKED) {
    $('#archive-input').val("");
    return;
  }

  var formData = new FormData();

  for (var i = 0; i < files.length; i++) {
    var file = files[i];

    formData.append('archives[]', file, file.name);
  }

  $("#modal-background").fadeIn(400);
  $("#modal-window").fadeIn(400);

  $.ajax({
    url: '/upload',
    type: 'POST',
    data: formData,
    processData: false,
    contentType: false,
    success: function(data){
        $("#modal-background").fadeOut(400);
        $("#modal-window").fadeOut(400);
    },
    xhr: function() {
      // create an XMLHttpRequest
      var xhr = new XMLHttpRequest();

      // listen to the 'progress' event
      xhr.upload.addEventListener('progress', function(evt) {

        if (evt.lengthComputable) {
          // calculate the percentage of upload completed
          var percentComplete = evt.loaded / evt.total;
          percentComplete = parseInt(percentComplete * 100);

          $("#current-percent").html(percentComplete);
          $("#current-progress-bar").progressbar('value', percentComplete);
        }

      }, false);

      return xhr;
    }
  });

  $('#archive-input').val("");
});

$("#from-time-cross").hide();
$("#to-time-cross").hide();

$("#from-time-cross").click(function() {
  $("#from-time").val("");
  $(this).fadeOut(400);
});

$("#to-time-cross").click(function() {
  $("#to-time").val("");
  $(this).fadeOut(400);
});

$("#from-time").datetimepicker({
  onSelectDate: function() {
    $("#from-time-cross").fadeIn(400);
  },
  step: 15,
  format:'Y-m-d H:i:s'
});

$("#to-time").datetimepicker({
  onSelectDate: function() {
    $("#to-time-cross").fadeIn(400);
  },
  step: 15,
  format:'Y-m-d H:i:s'
});

$(".help").hover(function() {
  $(this.nextElementSibling).fadeIn(200);
}, function() {
  $(this.nextElementSibling).fadeOut(200);
});

$("#current-progress-bar").progressbar({
  value: false,
  change: function() {

  }
});

$("#full-progress-bar").progressbar({
  value: false,
  change: function() {

  }
});

$("#start-button").click(function() {
  if ($(this).hasClass('disabled')) {
    return;
  }

  var formData = new FormData();

  formData.append('archives', JSON.stringify(ARCHIVES));
  
  if (CONFIGS.length > 0) {
    formData.append('config', 'custom');

    for (var i = 0; i < CONFIGS.length; i++) {
      var file = CONFIGS[i];

      formData.append('configs[]', file, file.name);
    }
  }

  else if ($("#config-current").prop('checked')) {
    formData.append('config', 'current');
  }

  else if ($("#config-archive").prop('checked')) {
    formData.append('config', 'archive');
  }

  if (ARCHIVES.length === 1) {
    var defaultFromDate = new Date($(".selected").attr('from'));
    var defaultToDate = new Date($(".selected").attr('to'));
    var defaultFrom = defaultFromDate.getTime() / 1000;
    var defaultTo = defaultToDate.getTime() / 1000;
    var from = $("#slider-range").slider("values", 0);
    var to = $("#slider-range").slider("values", 1);

    if (defaultFrom !== from) {
      formData.append('from', moment.unix(from).format("YYYY-MM-DD HH:mm:ss"));
    }

    if (defaultTo !== to) {
      formData.append('to', moment.unix(to).format("YYYY-MM-DD HH:mm:ss"));
    }
  }

  if ($("#method-udp").prop('checked')) {
    formData.append('method', 'udp');
  } else if ($("#method-socket").prop('checked')) {
    formData.append('method', 'websocket');
  } else {
    formData.append('method', 'none');
  }

  $("#modal-background").fadeIn(400);
  $("#modal-window").fadeIn(400);

  $.ajax({
    url: '/start',
    type: 'POST',
    data: formData,
    processData: false,
    contentType: false,
    success: function(data){
        
    },
    xhr: function() {
      // create an XMLHttpRequest
      var xhr = new XMLHttpRequest();

      // listen to the 'progress' event
      xhr.upload.addEventListener('progress', function(evt) {

        if (evt.lengthComputable) {
          // calculate the percentage of upload completed
          var percentComplete = evt.loaded / evt.total;
          percentComplete = parseInt(percentComplete * 100);

          $("#current-percent").html(percentComplete);
          $("#current-progress-bar").progressbar('value', percentComplete);
        }

      }, false);

      return xhr;
    }
  });
});

$("#tabs").click('li', function(element) {
  if (element.target.localName === 'i') return;

  var target = element.target.localName === 'span' ? element.target.parentElement : element.target;
  $(".active").removeClass('active');
  $(target).addClass('active');
  $("#right-panel").load($(target).children(":first").html() + '.html', function() {
    if (PAN_ZOOM) {
      PAN_ZOOM.destroy();
    }

    PAN_ZOOM = svgPanZoom('#map', {
      zoomScaleSensitivity: 0.3
    });

    onLoad($(target).html());
  });
});

socket.on('state', (value) => {
  $("#current-percent").html(Math.round(value.current));
  $("#full-percent").html(Math.round(value.full));

  $("#current-progress-bar").progressbar('value', value.current);
  $("#full-progress-bar").progressbar('value', value.full);

  $("#modal-operation").html(value.operation);

  if (value.current > 0) {
    $("#sensmap-link").fadeIn(400);
    $("#manager-link").fadeIn(400);
    $("#skip-button").fadeIn(400);
  } else {
    $("#sensmap-link").fadeOut(400);
    $("#manager-link").fadeOut(400);
    $("#skip-button").fadeOut(400);
  }
});

socket.on('lock', (value) => {
  LOCKED = value;

  if (value) {
    $("#file-list").hide();
    $("#file-list-lock").show();
    $(".start-btn").addClass('disabled');
    $(".upload-own > label").addClass('disabled');
  } else {
    $("#file-list").show();
    $("#file-list-lock").hide();
    $(".upload-own > label").removeClass('disabled');
  }
});

socket.on('count', (value) => {
  $("#user-count-number").html(value);
});

socket.on('done', () => {
  $("#modal-background").fadeOut(400);
  $("#modal-window").fadeOut(400);
});

socket.on('failed', (error) => {
  $("#modal-background").fadeOut(400);
  $("#modal-window").fadeOut(400, function() {
    $("#modal-error-text").html(error.error);
    $("#modal-error-window").fadeIn(400);
    $("#modal-background").fadeIn(400);
  });
});

socket.on('running', () => {
  $("#modal-background").fadeIn(400);
  $("#modal-window").fadeIn(400);
});

socket.on('archives', (value) => {

  $("#file-list").html("");
  $("#time-range").fadeOut(400);
  ARCHIVES = [];

  for (var file of value) {
    var listItem = $("<li>");
    listItem.hide();
    $("#file-list").append(listItem);

    listItem.click(function(e) {
      var name = $(this)[0].children[0].innerHTML;
      var index = ARCHIVES.indexOf(name);

      if (index > -1) {
        ARCHIVES.splice(index, 1);
        $(this).removeClass('selected');
      } else {
        ARCHIVES.push(name);
        $(this).addClass('selected');
      }

      if (ARCHIVES.length === 0) {
        $(".start-btn").addClass('disabled');
      }

      if (ARCHIVES.length > 0 && LOCKED === false) {
        $(".start-btn").removeClass('disabled');
      }

      if (ARCHIVES.length === 1) {
        $("#time-range").fadeIn(400);

        var fromTimeDate = new Date($(this).attr('from'));
        var toTimeDate = new Date($(this).attr('to'));
        
        var fromTime = fromTimeDate.getTime() / 1000;
        var toTime = toTimeDate.getTime() / 1000;

        $("#slider-range").slider({
          range: true,
          min: fromTime,
          max: toTime,
          step: 1,
          values: [fromTime, toTime],
          slide: function(e, ui) {
            var fromTimeDate = new Date(ui.values[0] * 1000);
            var toTimeDate = new Date(ui.values[1] * 1000);

            $("#from-time-text").html(fromTimeDate.toLocaleTimeString());
            $("#to-time-text").html(toTimeDate.toLocaleTimeString());
          }
        });

        $("#from-time-text").html(fromTimeDate.toLocaleTimeString());
        $("#to-time-text").html(toTimeDate.toLocaleTimeString());
      } else {
        $("#time-range").fadeOut(400);
      }
    });

    listItem.attr('from', file.from);
    listItem.attr('to', file.to);

    listItemName = $("<span>");
    listItemName.addClass("");
    listItemName.html(file.name);
    listItem.append(listItemName);

    var listItemCross = $("<i>");
    listItemCross.addClass('cross mdi mdi-close');
    listItemCross.click(function(e) {
      e.stopPropagation();
      var name = this.previousSibling.innerHTML;

      $.ajax({
        url: '/archive?name=' + name,
        type: 'DELETE',
        processData: false,
        contentType: false,
        success: function(data){
            
        }
      });
    })
    listItem.append(listItemCross);
    listItem.fadeIn(400);
  }
  
});

socket.on('files', (value) => {
  $("#right-panel").css("width", (document.body.clientWidth - 300) + "px");
  $("#tabs").html("");

  for (var i = 0; i < value.length; i++) {
    var name = value[i].slice(0, -5);
    var listItem = $('<li>');
    
    listItem.prop('title', name);

    if (i === 0) {
      listItem.addClass('active');
    }

    var listItemName = $('<span>');
    listItemName.html(name);

    listItem.append(listItemName);

    var listItemDelete = $('<i>');
    listItemDelete.addClass('file-cross mdi mdi-close');
    listItem.append(listItemDelete);

    listItemDelete.click(function() {
      var name = $(this.previousSibling).html() + ".html";

      $.ajax({
        url: '/file?name=' + name,
        type: 'DELETE',
        processData: false,
        contentType: false,
        success: function(data){
            
        }
      });
    });

    $("#tabs").append(listItem);
  } 

  if (value.length === 0) {
    if (PAN_ZOOM) {
      PAN_ZOOM.destroy();
    }

    PAN_ZOOM = undefined;
    
    $("#right-panel").html("");
  } else {
    $("#right-panel").load(value[0], function() {
      if (PAN_ZOOM) {
        PAN_ZOOM.destroy();
      }

      PAN_ZOOM = svgPanZoom('#map', {
        zoomScaleSensitivity: 0.3
      });

      onLoad(value[0]);
    });
  }
});

$("#error-close").click(function() {
  $("#modal-error-window").fadeOut(400);
  $("#modal-background").fadeOut(400);
});

$( window ).resize(function() {
  $("#right-panel").css("width", (document.body.clientWidth - 300) + "px");
});

function onLoad(file) {
  $(".legend").css('left','');
  $(".legend").css('right','');
  $(".legend").css('top','');

  var minimize = $("<i>");
  minimize.addClass('minimize mdi mdi-chevron-right');

  minimize.click(function() {
    if ($(this).hasClass('mdi-chevron-right')) {
      $(this).removeClass('mdi-chevron-right');
      $(this).addClass('mdi-chevron-left');
      $('.legend').animate({
        right: -290
      });
      $(this).animate({
        right: -10
      });
    } else {
      $(this).removeClass('mdi-chevron-lef');
      $(this).addClass('mdi-chevron-right');
      $('.legend').animate({
        right: 0
      });
      $(this).animate({
        right: 280
      });
    }
  });

  $('#right-panel').append(minimize);

  /*var info = $("<tbody>");
  info.html('<tr><td><b>Name</b></td><td colspan="2">ASDF.html</td></tr>');
  info.appendTo($('.legend'));*/

}