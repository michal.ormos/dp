var express = require("express");
var app = express();
var path = require("path")
var serveIndex = require('serve-index');
var shell = require('shelljs');

//app.use(express.static("mochawesome-report"));

var myLogger = function (req, res, next) {
shell.exec("cd /home/sewiortls/builds/*/*/michal.ormos/dp/mochawesome-report && tree -P 'test*' -H '.' -L 1 --noreport --charset utf-8 > index.html", {
    silent: false
  }).code;  	
next()
}

app.use(myLogger)

app.use(express.static('/home/sewiortls/builds/*/*/michal.ormos/dp/mochawesome-report'), serveIndex('mochawesome-report'))

app.listen(6210);
console.log("Server running at Port 6210");

