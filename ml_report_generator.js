/**
 * @file ml_report_generator.js

 * 
 * Payload tests for RTLS server

 * 
 * @author ormos
 * integration to mocha
 * 
 */

var binary = require('binary');
var crc = require('crc');
var net = require('net');
var request = require('request');

var tcpclient;
var mainIntervalCycle;

/* shared variables to mocha */
var DATAx = [];
var DATAy = [];
var DATA_object = [];
var DATA_string = "";

/* configurable*/
var SLOW_TAG = 2;
var BLINK_ERROR = 0; //nanoseconds
var SYNC_ERROR = 0; //nanoseconds
var ZONE_TOLERANCE = 0; //meters
var TAG_REFRESH_RATE = 300; //millisecs
var TIME_TO_START_TAGGING = 10; //seconds
var anchors = require('./Generated/anchors.json'); //anchor's clock offset and which ones to use?
var SERVER_IP = "192.168.225.2";
var API_KEY = "17254faec6a60f58458308763";
var NUMBER_OF_TAGS = 5;

/*don't touch*/
var SPEED_OF_LIGHT = 299702547;
var TIWA_MAX_TICKS = (128 * 499.2E6);
var MAX_PERIOD = 17.207401025625376;
var SEQ_PERIOD = 256;
var NUMBER_OF_SYNCS_PER_PERIOD = 4;
var ONE_NANOSECOND = 1e-9;
var TIME_BETWEEN_SYNC_FRAMES = 0.003; //milliseconds
var PERIOD = 1000; //second

/*****************************************/
exports.getData = function() {
    return DATA;
}

exports.callExit = function(command) {
	console.log("================ STOPPING REPORT GENERATOR ================");
	return command;
}

exports.closeClient = function() {
	clearInterval(mainIntervalCycle);
	tcpclient.end();
}
	

var getRandomInt = function(max) {
	var random = Math.floor(Math.random() * Math.floor(max));
	return random;
}

var getEnvelope = function(anchors) {
	var envelope = {
		left: Number.MAX_VALUE,
		bottom: Number.MAX_VALUE,
		right: -Number.MAX_VALUE,
		top: -Number.MAX_VALUE
	}

	console.log(anchors);

	for (var anchor in anchors) {

		var posX = parseFloat(anchors[anchor].pos[0]);
		var posY = parseFloat(anchors[anchor].pos[1]);

	    if (posX < envelope.left) {
	        envelope.left = posX;
	    }
	    if (posX > envelope.right) {
	        envelope.right = posX;
	    }
	    if (posY > envelope.top) {
	        envelope.top = posY;
	    }
	    if (posY < envelope.bottom) {
	        envelope.bottom = posY;
	    }
	}
	envelope.left -= ZONE_TOLERANCE;
	envelope.bottom -= ZONE_TOLERANCE;
	envelope.top += ZONE_TOLERANCE;
	envelope.right += ZONE_TOLERANCE;
	return envelope;
}

/**
 * buffer: 23 
 		   9c92 < CRC
 		   3d00 < len
 		   66 fe 61 39 80 d8 sewiortl<- anchor mac
 		   55                <- report type ascii (55 = U as universal)
 		   02 20 00          <- report universal header (2 blink) and length (20)
 		   bb                <- fcode
 		   ea 27 f0 5f 20 00 <- tag MAC
 		   13                <- seq (19)
 		   de12acde0d000000ca0bf6378800e933d535be18f6000ebb
 		   070400 <report universal header 
 		   ab308096 <anchor barometer
 		   030900 <report universal header
 		   0b7f7785960fff0281 <-extendend blink
 * parsed: [ { report: '|Presence|d8803961fe66|A|00205ff027ea|bb|000|19|0.932279049948668|0|3018|136|6334|748.14|-81.99|-80.37|0b7f7785960fff0281|ab308096|',
 * 
 * 
*/

var createBlinkReportBuffer = function (tagAddrB, anchorMacString, tagX, tagY, sequenceNumber, sysTime) {
	// tagX = getRandomInt(10);
	// tagY = getRandomInt(10);
	// console.log(tagX + " " + tagY);
	sysTime += calculateDistance(anchors[anchorMacString].pos[0], anchors[anchorMacString].pos[1], tagX, tagY) / SPEED_OF_LIGHT;
	
	var convTime = ("0000000000" + Math.round((sysTime * TIWA_MAX_TICKS)).toString(16)).substr(-10).match(/.{1,2}/g);
	var convTimeB = new Buffer([
		parseInt(convTime[4], 16),
		parseInt(convTime[3], 16),
		parseInt(convTime[2], 16),
		parseInt(convTime[1], 16),
		parseInt(convTime[0], 16), 		
		0x00,
		0x00,
		0x00]);

	var anchorAddr = anchorMacString.match(/.{1,2}/g);
	var anchorAddrB = new Buffer([parseInt(anchorAddr[5], 16),
		parseInt(anchorAddr[4], 16),
		parseInt(anchorAddr[3], 16),
		parseInt(anchorAddr[2], 16),
		parseInt(anchorAddr[1], 16),
		parseInt(anchorAddr[0], 16)]);
	// var tagAddrB = new Buffer([0x01,0x02,0x03,0x04,0x05,0x06]);

	var universalType = new Buffer([0x55]);
	var universalHeader = new Buffer([0x02,0x20,0x00]);
	var fcodeB = new Buffer([0xbb]);
	var seqNumberB = new Buffer([sequenceNumber]);

	var uwbPayloadB = new Buffer([/*0x78, 0x1e, 0x55, 0xb8, 0x30, 0xfd, 0xc2, 0x00, *///uwb TS + sync group seq num
								  0x00, 0x30, //max noise
								  0x14, 0x90, //first_path_amp1
								  0x3c, 0xb8, //std_noise
								  0x00, 0xe3, //first_path_amp2
								  0x41, 0xdc, //first_path_amp3
								  0x3b, 0x06, //max_growth_cir
								  0x42, 0x51, //rx_pream_count 
								  0x03, 0x1a]); //firstpath_index


	return Buffer.concat([anchorAddrB, universalType, universalHeader, fcodeB, tagAddrB, seqNumberB, convTimeB, uwbPayloadB]);
}


var calculateDistance = function(x1, y1, x2, y2) {
    return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

var timeOfFlight = function(anchor1, anchor2) {
	return calculateDistance(anchors[anchor1].pos[0], anchors[anchor1].pos[1], anchors[anchor2].pos[0], anchors[anchor2].pos[1]) / SPEED_OF_LIGHT;
}

/**
 * 1f 00 <-len
 * 51 53 62 39 80 d8 <- anchor mac
 * 55 <- universal 
 * 00 11 00  <-report universal header (0 emission, 11 length)
 * aa <-fcode 
 * 51 53 62 39 80 d8 <- master mac
 * 78 1e 55 b8 30 fd c2 00 00 00 06 01 00 3b>
 * 
 * 
*/

/** slave
 * 36 00
 * 66 fe 61 39 80 d8 
 * 55 
 * 01 21 00
 * aa
 * 51 53 62 39 80 d8 
 * bc 6f 65 6b 96 fa 7c 00 00  //uwb TS + sync group seq
 * 00 30 //max noise
 * 14 90 //first_path_amp1
 * 3c b8 //std_noise
 * 00 3e //first_path_amp2
 * 41 dc //first_path_amp3
 * 3b 06 //max_growth_cir
 * 42 51 //rx_pream_count
 * 03 1a //firstpath_index
unpack U msgstring: |d8803961fe66|A|d88039625351|aa|111|188|7c|fa|96|6b|65|0|5168|15504|184|16702|15324|16902|849|748.26||
*/
var createSyncReportBuffer = function (anchorMacString, master, masterMacString, sequenceNumber, sysTime) {
	sysTime += timeOfFlight(anchorMacString, masterMacString);
	var convTime = ("0000000000" + Math.round((sysTime * TIWA_MAX_TICKS)).toString(16)).substr(-10).match(/.{1,2}/g);
	var convTimeB = new Buffer([
		parseInt(convTime[4], 16),
		parseInt(convTime[3], 16),
		parseInt(convTime[2], 16),
		parseInt(convTime[1], 16),
		parseInt(convTime[0], 16), 		
		0x00,
		0x00,
		0x00]);

	var anchorAddr = anchorMacString.match(/.{1,2}/g);
	var anchorAddrB = new Buffer([parseInt(anchorAddr[5], 16),
		parseInt(anchorAddr[4], 16),
		parseInt(anchorAddr[3], 16),
		parseInt(anchorAddr[2], 16),
		parseInt(anchorAddr[1], 16),
		parseInt(anchorAddr[0], 16)]);
	var masterAddr = masterMacString.match(/.{1,2}/g);
	var masterAddrB = new Buffer([parseInt(masterAddr[5], 16),
		parseInt(masterAddr[4], 16),
		parseInt(masterAddr[3], 16),
		parseInt(masterAddr[2], 16),
		parseInt(masterAddr[1], 16),
		parseInt(masterAddr[0], 16)]);

	var universalType = new Buffer([0x55]);
	var universalHeader = new Buffer([master ? 0x00 : 0x01, master ? 0x11 : 0x21, 0x00]);
	var fcodeB = new Buffer([0xaa]);
	var seqNumberB = new Buffer([sequenceNumber]);
	var groupSeqNumberB = new Buffer([0x00]);

	if (master) {
		var uwbPayloadB = new Buffer([/*0x78, 0x1e, 0x55, 0xb8, 0x30, 0xfd, 0xc2, 0x00, */]);
	} else {
		var uwbPayloadB = new Buffer([/*0x78, 0x1e, 0x55, 0xb8, 0x30, 0xfd, 0xc2, 0x00, *///uwb TS + sync group seq num
									  0x00, 0x30, //max noise
									  0x14, 0x90, //first_path_amp1
									  0x3c, 0xb8, //std_noise
									  0x00, 0xe3, //first_path_amp2
									  0x41, 0xdc, //first_path_amp3
									  0x3b, 0x06, //max_growth_cir
									  0x42, 0x51, //rx_pream_count 
									  0x03, 0x1a]); //firstpath_index


	}
	return Buffer.concat([anchorAddrB, universalType, universalHeader, fcodeB, masterAddrB, seqNumberB, groupSeqNumberB, convTimeB, uwbPayloadB]);
}

var startTag = function(envelope, tcpclient, sysTime, delimB, tagAddrB, callback) {
	console.log("[startTag]");
	var blinkSeq = 0;
	var counter = 1;
	var messages_counter = 0;
	var last_sign = -1;
	var last_y = envelope.bottom
	var startmod = Math.abs(envelope.bottom % 2);
	var tagAddrB;
	console.log(" ==== Generating Positions and Reports ==== ");
	for (var y = envelope.bottom; y <= envelope.top; y++) {

		if (Math.sign(y) != last_sign) {
			y = Math.abs(last_y);
		}

		last_y = y;
		last_sign = Math.sign(y);

		// float comparision round number, we have more than 10 decimal places
		// we can't use straight ===
		if (Math.abs(y % 2) <= startmod + 0.00001 && 
			Math.abs(y % 2) >= startmod - 0.00001) { //right-to-left
			var startX = envelope.right;
			var step = -1;
			var endX = envelope.left;
		} else { //ltr
			var startX = envelope.left;
			var step = 1;
			var endX = envelope.right;
		}
		for (var x = startX; step === -1 ? x >= endX : x <= endX; x += step) {
			setTimeout(function(connector, x, y) {
				for (var anchor in anchors) {
					var report = createBlinkReportBuffer(
								tagAddrB,
								anchor,
								x, y,
								blinkSeq, //sequence number
								(sysTime + anchors[anchor].clockOffset + TAG_REFRESH_RATE + 
								(Math.random() < 0.5 ? -1 : 1) * BLINK_ERROR * Math.random() * ONE_NANOSECOND)
							)
					var lenB = new Buffer((report.length.toString('16') + "0000").substr(0, 4), 'hex'); 
					var reportWithLength = Buffer.concat([lenB, report]);
					var crcInt = crc.crc16ccitt(reportWithLength);
					var crcB = new Buffer([(crcInt & 0xFF), ((crcInt >> 8) & 0xFF)])

					var message = Buffer.concat([delimB, crcB, reportWithLength]);
					if (anchor == 'D88000000001') {	
						DATA_object.push({x: x, y: y});
         
						var pre_string = "";
						pre_string = pre_string.concat(x);
						pre_string = pre_string.concat(",");
						pre_string = pre_string.concat(y);
						pre_string = pre_string.concat(" ");
						DATA_string = DATA_string.concat(pre_string);              

						DATAx.push(x);
						DATAy.push(y);
					}
					connector.write(message);
				}
				blinkSeq = (blinkSeq + 1) % SEQ_PERIOD;
				messages_counter++;
				if ( messages_counter == Math.round(counter/4)) {
					console.log("25%");
				} else if (messages_counter == Math.round(counter/2)) {
					console.log("50%");
				} else if (messages_counter == Math.round(3*counter/4)) {
					console.log("75%");
				} else if ( messages_counter >= counter-1) {
					callback();
				}
			}, counter * TAG_REFRESH_RATE * SLOW_TAG + getRandomInt(10) , tcpclient, x, y);
			counter++;
		}
	}
};

var parsePosition = function (feed) {
    var position = [];

    for (var i = 0; i < feed.datastreams.length; i++) {
        if (feed.datastreams[i].id === "posX" && !isNaN(parseFloat(feed.datastreams[i].current_value))) {
            position[0] = feed.datastreams[i].current_value;
        }
        if (feed.datastreams[i].id === "posY" && !isNaN(parseFloat(feed.datastreams[i].current_value))) {
            position[1] = feed.datastreams[i].current_value;
        }
        if (feed.datastreams[i].id === "posZ" && !isNaN(parseFloat(feed.datastreams[i].current_value))) {
            position[2] = feed.datastreams[i].current_value;
        }
    };
    if (position.length < 2) {
    	console.log("Anchors do not have set position");
    	process.exit(1);
    }
    return position;
}

var start = function(callback) {
	tcpclient = new net.Socket();
	var envelope = getEnvelope(anchors);

	console.log(envelope);

	var delimB = new Buffer([0x23]);
	var syncSeq = 0;
	var sysTime = 0;
	var startedTag = false;
	var counter = 0;
	tcpclient.connect(5000, SERVER_IP, function() {
		mainIntervalCycle = setInterval(function() {
			sysTime = (sysTime + 1) % MAX_PERIOD;
			if (sysTime > TIME_TO_START_TAGGING && !startedTag) {
				for ( var i = 0; i < NUMBER_OF_TAGS;  i++) {
					var tagAddrB = new Buffer([i,0x02,0x03,0x04,0x05,0x06]);
					startTag(envelope, tcpclient, sysTime, delimB, tagAddrB, function() {
						callback()
					});
					startedTag = true;
				}
			}
			for (var i = 0; i < NUMBER_OF_SYNCS_PER_PERIOD; i++) {
				var master = true;
				var masterMac;
				for (var anchor in anchors) {
					if (master) {
						masterMac = anchor;
					}
					var report = createSyncReportBuffer(
								anchor,
								master, masterMac,
								syncSeq, //sequence number
								(sysTime + 
									anchors[anchor].clockOffset + 
									i * TIME_BETWEEN_SYNC_FRAMES + 
									(Math.random() < 0.5 ? -1 : 1) * SYNC_ERROR * Math.random() * ONE_NANOSECOND)  
							)
					var lenB = new Buffer((report.length.toString('16') + "0000").substr(0, 4), 'hex'); 
					var reportWithLength = Buffer.concat([lenB, report]);
					var crcInt = crc.crc16ccitt(reportWithLength);
					var crcB = new Buffer([(crcInt & 0xFF), ((crcInt >> 8) & 0xFF)])
					var message = Buffer.concat([delimB, crcB, reportWithLength]);
					tcpclient.write(message);
					master = false;

				}
				syncSeq = (syncSeq + 1) % SEQ_PERIOD;

			}
			counter++;
		}, PERIOD)
	});
}

module.exports.main = function main(server_ip, blink_err, tag_refresh_rate, sync_err, number_of_tags, slow_tag, callback) {
	BLINK_ERROR = blink_err;
	SYNC_ERROR = sync_err;
	SERVER_IP = server_ip;
	TAG_REFRESH_RATE = tag_refresh_rate;
	NUMBER_OF_TAGS = number_of_tags;
	SLOW_TAG = slow_tag;
	console.log("================ STARTING REPORT GENERATOR ================");
	request({
			method: "GET",
			// json: true,
			uri: "http://" + SERVER_IP + "/sensmapserver/api/anchors",
			// "rejectUnauthorized": false, //this is fine since sensmapserver and rtlsmanager are on localhost. Once moved to separate machines, ssl is needed
			headers: {
				'X-ApiKey': "17254faec6a60f58458308763"
			} 
		}, function(error, response, body) {
			var feeds = JSON.parse(body);
			for (var i = 0; i < feeds.results.length; i++) {
				if (feeds.results[i].title !== undefined && feeds.results[i].title[0] == "0" && feeds.results[i].title[1] == "x") {
					var mac = feeds.results[i].title.slice(2);//.toLowerCase();
					if (anchors[mac]) {
						anchors[mac].pos = parsePosition(feeds.results[i]);
					}
				}
			}
			start(function() {
				console.log(" ==== DONE ==== ");
				callback(DATAx, DATAy, DATA_object, DATA_string);
				// setTimeout(function(){ 
				// 	console.log("================ STOPING REPORT GENERATOR ================");
				// 	// process.exit(0); 
				// 	tcpclient.end();
				// }, 6000);
			});
		});
	}