"use strict"

/**
 * @file payload_tests.js
 * 
 * @dependencies @file arete.js, data.json
 * @dependencies modules mocha, request, should, supertest
 * 
 * Payload tests for RTLS server
 *
 * @author ormos
 */

 /**
  * This is payload test script divided to 8 parts
  * 1) Building creating (POST)
  * 2) Create feeds(tags) (POST)
  * 3) Get feeds (GET)
  * 4) Request feeds with min message (PUT)
  * 5) Request feeds with max message (PUT)
  * 6) Arete payload test (PUT)
  * 7) Batch test 200 (PUT)
  * 8) Delete feeds/tags
  * 9) Delete building
  */

 /**
  * Sewio module @Arete is important here. It performing all statistical jobs. 
  * From measuring the times between requests to running the jobs separately,
  * with desired delay and concurency. Input of arete is whole function.
  * It also print all the statistic information separetely.
  * Mocha pass done/callback to arete
  */

 /** 
  * @supertest is used to create port connection with sensmapserver to @api
  * @assert and @should are mocha tools for easy test comparisions
  * */ 

 /**
  * Data are stored here in variables or generated to data.json @file
  */

 var configIni = require('config.ini');
 var conf = configIni.load('./config.ini');  

var node_ssh, ssh;
node_ssh = require('node-ssh')
ssh = new node_ssh()    

var IP = conf.SectionOne.IP_ADDRESS;
const SERVER_IP = IP;
const SERVER_URL = "http://" + SERVER_IP + "/sensmapserver";
const SERVER_LOGIN = "sewiortls";
const SERVER_PASSWORD = "sensmap";
const addContext = require('mochawesome/addContext');

var supertest = require('supertest'),
    arete = require('./arete.js'),
    api = supertest('http://' + IP + '/sensmapserver'),
    assert = require('assert'),
    should = require('should'),    
    DEBUG = false,
    report2 = 0;

    var buildingID;    
    var json = require('../data.json'); //with path

    var building = {
        "id": "42",
        "alias": "test building",
        "title": "Building:kancl",
        "private": "false",
        "description": "This is virtual test building",
        "feed": "Another description in another value",
        "status": "live",
        "updated": "2018-08-23 12:00:00.776046",
        "created": "2018-08-23 12:00:00.776046",
        "creator": "rtlsuser",
        "version": "1.0.0",
        "website": "www.websiteforthisfeed.com",
        "tags": [
          "brno"
        ],
        "location": {
          "disposition": "fixed",
          "ele": "kancl",
          "name": "Building:kancl",
          "lat": "0",
          "exposure": "",
          "lon": "0",
          "domain": ""
        }
      }

    var more_data = {
        'posZ': {
            "id":"posZ",
            "current_value":"0",
            "at":"2016-06-11 18:24:07.597976"
        },
        'quaternion': {
            "id":"quaternion",
            "current_value":"12.25;13.55;14.25;15.35",
            "at":"2016-06-11 18:24:07.597976"
        },
        'acc': {
            "id":"acc",
            "current_value":"12;15;20",
            "at":"2016-06-11 18:24:07.597976"
        },
        'gyro': {
            "id":"gyro",
            "current_value":"12;15;20",
            "at":"2016-06-11 18:24:07.597976"
        },
        'mag': {
            "id":"mag",
            "current_value":"12;15;20",
            "at":"2016-06-11 18:24:07.597976"
        },
        'barometer temp': {
            "id":"temperature",
            "current_value":"12;15;20",
            "at":"2016-06-11 18:24:07.597976"
        },
        'barometer pressure': {
            "id":"calibratedPressure",
            "current_value":"12;15;20",
            "at":"2016-06-11 18:24:07.597976"
        }
    };

(conf.SectionTwo.PAYLOAD_API_TEST ? describe : describe.skip)("Payload Tests API", function() {
    describe("Initialization of Building", function(){
        it('Create building', function(done) {
            api.post('/api/buildings')
                .set({
                    'X-APIKey': '17254faec6a60f58458308763'
                })
                .send(building)
                .end(function(err, res) {
                    if (err) {
                        throw err;
                    }
                    res.status.should.be.equal(200)
                    buildingID = res.body['id'];
                    done();
                });
        });         
    });

    describe('Main test', function(){
        
        var num_of_feeds = conf.PayloadTestAPI.NUMBER_OF_FEEDS; // number of feed that will be creater
        var num_of_iterations = conf.PayloadTestAPI.ITERATIONS; // number of (PUT) iterations one feed will do
        var concurency = conf.PayloadTestAPI.CONCURENCY; // concurency of server
        var delay = conf.PayloadTestAPI.DELAY; // in miliseconds | timer between PUT is send, one by one
        var ID_test = [];

        for (var index = 0; index < num_of_feeds; index++) {
            ID_test[index] = index;
        }

        ID_test.forEach(function (id) {
            it('Creating (POST) tag', function(done){
                api.post('/api/feeds')
                .set({
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'X-ApiKey': '17254faec6a60f58458308763'
                })  
                .send({
                    'id': id,
                    'type': "tag",
                    'title': "test_tag_" + id,
                    'address': Math.floor(Math.random() * 0xffffffffff).toString(16),
                    'datastreams':[
                        {"id":"posX","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"},
                        {"id":"posY","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"},
                        {"id":"clr","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"}],
                    "location": {
                        "disposition": "",
                        "ele": "kancl",
                        "name": "Building:kancl",
                        "lat": "0",
                        "exposure": "",
                        "lon": "0",
                        "domain": ""
                    }                 
                })
                .expect('Content-Type', /json/)
                .expect(200)
                .end(function (err, res) {
                    if (DEBUG) {
                        console.log("Stage1")
                        console.log(res.body['id']);
                    }
                    ID_test[id] = res.body['id'];
                    res.status.should.be.equal(200);
                    done();
                }); 
            });
        });

        ID_test.forEach(function (id) {
            it('Requesting (GET) tag', function(done){
                if (DEBUG) {
                    console.log("Stage3");
                    console.log(id);                              
                }
                api.get('/api/feeds/' + ID_test[id])
                    .set({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'X-ApiKey': '17254faec6a60f58458308763'
                    })                  
                    .expect(200)
                    .end(function(err, res) {
                        if (DEBUG) {
                            console.log(res);
                        }
                        res.status.should.be.equal(200);                        
                        done();
                    });                                                 
            });  
        }); 

        ID_test.forEach(function (id) {
            it('Requesting (PUT) tag | SHORT (minimal) message', function(done){
                if (DEBUG) {
                    console.log("Stage3");
                    console.log(id);                              
                }
                api.put('/api/feeds/' + id)
                    .set({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'X-ApiKey': '17254faec6a60f58458308763'
                    })  
                    .send({
                        'id': id,
                        'type': "tag",
                        'address': Math.floor(Math.random() * 0xffffffffff).toString(16),
                        'datastreams':[
                            {"id":"posX","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"},
                            {"id":"posY","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"},
                            {"id":"clr","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"}],
                    })                     
                    .expect(200)
                    .end(function(err, res) {
                        if (DEBUG) {
                            console.log(res);
                        }
                        done();
                    });                                                 
            });  
        });    
        
        ID_test.forEach(function (id) {
            it('Requesting (PUT) tag | FULL (maximal) message', function(done){
                if (DEBUG) {
                    console.log("Stage3");
                    console.log(id);                              
                }
                api.put('/api/feeds/' + id)
                    .set({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'X-ApiKey': '17254faec6a60f58458308763'
                    })  
                    .send({
                        'id': id,
                        'type': "tag",
                        'address': Math.floor(Math.random() * 0xffffffffff).toString(16),
                        'datastreams':[
                            {"id":"posX","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"},
                            {"id":"posY","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"},
                            {"id":"clr","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"}],
                        more_data
                    })                     
                    .expect(200)
                    .end(function(err, res) {
                        if (DEBUG) {
                            console.log(res);
                        }
                        done();
                    });                                                 
            });  
        });         
        
        it('Requesting (PUT) ' + num_of_feeds + ' tags | for ' + num_of_iterations
        + ' iterations | with delay ' + delay + ' | and concurency ' + concurency, 
        function(done){
            arete.loadTest({
                name: 'first payload test',
                timer: delay,                
                feeds: num_of_feeds,
                requests: num_of_iterations,
                concurrentRequests: concurency,
                targetFunction: function(callback) {              
                    ID_test.forEach(function (id) {
                        if (DEBUG) {
                            console.log("Stage3");
                            console.log(id);                              
                        }
                        api.put('/api/feeds/' + id)
                            .set({
                                'Content-Type': 'application/json',
                                'Accept': 'application/json',
                                'X-ApiKey': '17254faec6a60f58458308763'
                            })  
                            .send({
                                'id': id,
                                'type': "tag",
                                'address': Math.floor(Math.random() * 0xffffffffff).toString(16),
                                'datastreams':[
                                    {"id":"posX","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"},
                                    {"id":"posY","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"},
                                    {"id":"clr","current_value":Math.floor(Math.random() * (1000 - 100) + 100) / 100,"at":"2016-06-11 18:24:"+Math.floor(Math.random() * 50 + 9)+".597976"}],
                            })                     
                            .expect(200)
                            .end(function(err, res) {
                                if (DEBUG) {
                                    console.log(res);
                                }
                                callback(err, res);
                            });                                                 
                    });
                },
                printResponses: false,
                printReport: true,
                printSteps: false,
                callback: function(error, report) {		
                    report2 = report;		
                    assert(report.successfulResponses.length > report.results.length/2, "We didn't get at least half successful responses!");
                    assert(report.averageResponseTimeInterval < 200, "Time between responses is more than 200ms!");
                    assert(report.timeElapsed < 120000, "Unacceptable amount of time for 1000 requests to complete: more than 110 seconds");
                    done();
                }
            });                       
        });      

        it('RESULTS Requesting (PUT) ' + num_of_feeds + ' tags',
        function(done){        
            addContext(this, 'Number of send responses: ' + report2.results.length);
            addContext(this, 'Number of successful accepted responses: ' + report2.successfulResponses.length);
            addContext(this, 'Average response time interval [ms]: ' + report2.averageResponseTimeInterval);
            addContext(this, 'Time elapsed [ms]: ' + report2.timeElapsed);

            done();
        });


        ID_test.forEach(function (id) {
            it('Requesting (PUT) tag for Batch (200) requests',
            function(done){
                api.put('/api/feeds/' + ID_test[id])
                    .set({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'X-ApiKey': '17254faec6a60f58458308763'
                    })  
                    .send(json)                        
                    .end(function (err, res) {
                        if (err) throw err;
                        if (DEBUG) {
                            // console.log(res);
                            console.log(res);
                        }
                        res.status.should.be.equal(200)
                        done();
                    });               
            });                     
        });   

        ID_test.forEach(function (id) {
            it('DELETE tag', function(done){
                if (DEBUG) {
                    console.log("Stage4");
                    console.log(ID_test[id]);
                }
                api.delete('/api/feeds/' + ID_test[id])
                    .set({
                        'Content-Type': 'application/json',
                        'Accept': 'application/json',
                        'X-ApiKey': '17254faec6a60f58458308763'
                    })  
                    .expect(200)
                    .end(function (err, res) {
                        res.status.should.be.equal(200)
                        done();
                    });               
            });                     
        });      

        describe("Delete building", function(){
            it('should delete building', function(done) {
                api.delete('/api/buildings/' + buildingID)
                    .set({
                        'X-APIKey': '17254faec6a60f58458308763'
                    })
                    .end(function(err, res) {
                        if (err) {
                            throw err;
                        }
                        res.status.should.be.equal(200)
                        done();
                    });
            });         
        });
        it('Restore DB to state before test',function(done) {
            ssh.connect({
                host: SERVER_IP,
                username: SERVER_LOGIN,
                password: SERVER_PASSWORD
            })

            .then(function() {
                ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S nodejs /builds/michal.ormos/dp/restore_db.js').then(function(result) {
                    console.log(result);  
                    ssh.dispose();  
                    done();
                });                                                
            });
        });     

        it('Restart RTLS server',function(done) {
            ssh.connect({
                username: SERVER_LOGIN,
                host: SERVER_IP,
                password: SERVER_PASSWORD
            })
            .then(function() {                        
                ssh.execCommand('echo ' + SERVER_PASSWORD + ' | sudo -S service rtlsserver restart').then(function(result) {
                    console.log(result);
                    ssh.dispose();
                    // wtf.dump()
                    done();
                });                                              
            });
        });   
    });
});