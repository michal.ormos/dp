"use strict"

/**
 * @file arete.js
 * 
 * Statistical module for payload testing
 * 
 * @author ormos
 */

var _ = require('underscore');
var execTime = require('exec-time');
var util = require('util');
var mockery = require('mockery');
var http = require('http');
var https = require('https');
var clone = require('clone');

// Use mockery to force the agent option of http.request
// and https.request by mocking http and https and overriding request

mockery.enable({
    warnOnReplace: false,
    warnOnUnregistered: false
});

var httpsAgent = new https.Agent();
var httpAgent = new http.Agent();
var log = require('log-to-file');

httpsAgent.maxSockets = 10;
httpAgent.maxSockets = 10;

var httpMock = clone(http);
httpMock.request = function (options, cb) {
	options.agent = httpAgent;
	return http.request(options, cb);
}

var httpsMock = clone(https); 
httpsMock.request = function(options, cb) {
	options.agent = httpsAgent;
	return https.request(options, cb);
}

mockery.registerMock('http', httpMock);
mockery.registerMock('https', httpsMock);

function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
	  if ((new Date().getTime() - start) > milliseconds){
		break;
	  }
	}
  }
  

// executes requests, calculate stat report, print to console
function loadTest(config) {
	if (!config) throw new Error("Missing argument config");

	// set default values, if properties not already set
	_.defaults(config, {
		printResponses: true,
		printReport: true,
		printSteps: true,
		name: 'default',
		timer: 0,
		feeds: 1,
		concurrentRequests: 10
	})

	httpAgent.maxSockets = config.concurrentRequests;
	httpsAgent.maxSockets = config.concurrentRequests;

	executeRequests(config.name, config.timer, config.feeds, config.requests, config.printResponses, config.printSteps, config.targetFunction, function(results) {
		var report = calculateStats(results);
 		
 		if (config.printReport) outputReport(report);

 		config.callback(null, report);
	});
}

function executeRequests(testName, timer, numFeeds, numIterations, printResponses, printSteps, targetFn, callback) {
	var results = [];

	// start timer
	var profiler = new execTime(testName, printSteps, 'ms', false);
	profiler.beginProfiling();
	profiler.step('Starting to make requests');

	_.range(numIterations).forEach(function(id) {
		profiler.step('Sending request #' + id);

		targetFn(function(err, data) {
			var timeSinceLastResponse = profiler.elapsedSinceLastStep();
			profiler.step('Received response for request #' + id);

			log(data);
			log(err);

			// log result:
			results.push({
				requestId: id, 
				success: err ? false : true,
				response: err || data, 
				timeSinceBeginning: profiler.elapsedSinceBeginning(),
				timeSinceLastResponse: timeSinceLastResponse,
				numFeeds: numFeeds,
				numIterations: numIterations,
				name: testName,
				timer: timer
			});
			
			if (printResponses) console.log(err || data);

			if (results.length == numFeeds*numIterations) {
				callback(results);
			}

			sleep(timer);
		});
	});

	profiler.step('All requests fired off');
}

// calculate some statistics based on results.  all times in ms.
function calculateStats(results) {
	var report = {
		results: results,
		longestResponseTimeInterval: _.max(results, function(result){ return result.timeSinceLastResponse;}).timeSinceLastResponse,
		shortestResponseTimeInterval: _.min(results, function(result){ return result.timeSinceLastResponse;}).timeSinceLastResponse,
		averageResponseTimeInterval: _.reduce(results, function(prev, curr){ return prev + curr.timeSinceLastResponse; }, 0) / results.length,
		successfulResponses: _.where(results, {success: true}),
		shortestResponseTime: _.min(results, function(result){ return result.timeSinceBeginning;}).timeSinceBeginning,
		longestResponseTime:  _.max(results, function(result){ return result.timeSinceBeginning;}).timeSinceBeginning,
		averageResponseTime: _.reduce(results, function(prev, curr){ return prev + curr.timeSinceBeginning; }, 0) / results.length,
		timeElapsed: _.max(results, function(result){ return result.timeSinceBeginning;}).timeSinceBeginning
	};

	return report;
}

// Print out stat report
function outputReport(report) {
	// console.log("Response log:", results);
	// addContext(this, '[REPORT] Requests fired: ' + report.results.length);
	// addContext(this, '[REPORT] Feeds: ' + report.results[0].numFeeds);
	// addContext(this, '[REPORT] Number of Iterations per feed: ' + report.results[0].numIterations);
	// addContext(this, '[REPORT] Delay: ' + report.results[0].timer);
	// addContext(this, '[REPORT] Successful responses: ' + report.successfulResponses.length);
	// addContext(this, '[REPORT] Successful resposne rate: ' + (report.successfulResponses.length / report.results.length * 100).toFixed(2));

	console.log("\t=========================================================== ");
	console.log("\t=== Sewio Payload Test Stats for " + report.results[0].name  + " === ");
	console.log("\t=========================================================== ");
	console.log(util.format('\t%s requests fired. \n\t%s feeds, each with %s iteration(s) and %sms delay. \n\tOf which we got back %s successful responses (%s% success rate)', 
		report.results.length,
		report.results[0].numFeeds,
		report.results[0].numIterations,
		report.results[0].timer,
		report.successfulResponses.length,
		(report.successfulResponses.length / report.results.length * 100).toFixed(2)
	));
	console.log(' ');
	console.log('\tLongest time between responses: ', report.longestResponseTimeInterval, 'ms');
	// console.log('\tShortest time between responses: ', report.shortestResponseTimeInterval, 'ms');
	console.log('\tAverage response time interval: ', report.averageResponseTimeInterval , 'ms');
	console.log(' ');
	console.log('\tShortest response time: ', report.shortestResponseTime, 'ms');
	// console.log('\tLongest response time: ', report.longestResponseTime, 'ms');
	console.log('\tAverage response time: ', report.averageResponseTime , 'ms');
	console.log(' ')
	console.log('\tTotal test duration: ', report.timeElapsed, 'ms');
	console.log("\t=========================================================== ");
}

module.exports = {
	loadTest: loadTest
};