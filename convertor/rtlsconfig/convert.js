/**
 * script to convert old config.ini to new one
 * 1) keeps old values (e.g. keeps set threshold, udp broadcast address, etc.)
 * 2) removes no longer existing parameters
 * 3) moves parameters to different categories
 * 4) adds new parameters
 *
 * @return 0 if everything goes as expected 
 *         1 on any error, warning, incosistency, unexpected situation) 1
 *         on other error -> system specific code (e.g. for reading nonexisting file - 8, for reading undefined - 8, etc)
 */ 

process.exitCode = 0; 

var fs = require('fs');
var oldConfigPath = process.argv[2]; // original config.ini
var newConfigPath = process.argv[3]; // new config.ini
var oldConfigString = fs.readFileSync(oldConfigPath, 'utf8');
var newConfigString = fs.readFileSync(newConfigPath, 'utf8');

/**
 * parses file string into object {parameter:{value:x, category:y}, parameter2:...}
 */
 
var configstringToObject = function(configstring) {
    var configObject = {};
    var lines = configstring.split(/\r?\n/);
    var category = "";
    var parameter = "";
    var value = "";
    for (var i = 0; i < lines.length; i++) {
        parameter = "";
        value = "";
        var categoryRegexp = /\[([^\]]*)]/g;
        var match = categoryRegexp.exec(lines[i]);

        if (match !== null && match[1] !== undefined) {
            category = match[1];
        }

        var parameterRegexp = /([^=]*)=(.*)$/g;
        var match = parameterRegexp.exec(lines[i]);

        if (match !== null && match[1] !== undefined && match[2] !== undefined) {
            parameter = match[1];
            value = match[2];
            if (category === "") {
                console.log("Missing category name");
                process.exitCode = 1;
            }
            configObject[parameter] = {};
            configObject[parameter].value = value;
            configObject[parameter].category = category;
        }
    }

    return configObject;
}

/**
 * creates (sorted) array of categories from top to down
 */

var configstringToCategoriesArray = function(configstring) {
    var configObject = {};
    var lines = configstring.split(/\r?\n/);
    var category = "";
    var categories = [];
    for (var i = 0; i < lines.length; i++) {
        var categoryRegexp = /\[([^\]]*)]/g;
        var match = categoryRegexp.exec(lines[i]);
        if (match !== null && match[1] !== undefined) {
            category = match[1];
            categories.push(category);
        }

    }
    return categories;
}


/**
 * update in situ 
 * forgets parameters that were in old config and are no longer in new config
 * if found matching parameters, keeps the old values
 * does not care about parameter's category
 */

var updateConfigObject = function (oldConfigObj, newConfigObj) {
   for (var newparam in newConfigObj) {
       for (var oldparam in oldConfigObj) {
           if (oldparam === newparam) {
               newConfigObj[newparam].value = oldConfigObj[newparam].value;
           }
       }
   } 
}



var currConfig = configstringToObject(oldConfigString);
var newConfig = configstringToObject(newConfigString);
var categories = configstringToCategoriesArray(newConfigString);
updateConfigObject(currConfig, newConfig); 


var updatedConfigString = "";

/**
 * moves parameters to new categories
 */

for (var c = 0; c < categories.length; c++) {
    updatedConfigString += "["+categories[c] + "]\n";
    for (var newparam in newConfig) {
        if (newConfig[newparam].category === categories[c]) {
            updatedConfigString += newparam + "=" + newConfig[newparam].value + "\n";
        }
   } 
}

/**
 * output to stdout
 */

console.log(updatedConfigString);
