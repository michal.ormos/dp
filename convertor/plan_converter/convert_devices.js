var request = require('request');

getFeeds = function(callback) {
    request({
        method:"GET",
        uri: "http://localhost/sensmapserver/api/feeds", 
        headers: {
            'X-ApiKey': "17254faec6a60f58458308763"
            }
        }, function(error, response, body) {

            try {
                var feeds = JSON.parse(body);
                callback(feeds);
            } catch (e) {
                process.exitCode = 1;
                console.error("Error while parsing response from /api/feeds", body);
            }
    });
}



updateFeed = function(id, data) { 
 //   console.log("updating building",bid,"with",data);
    request({
        method: "PUT",
        json: true,
        uri: "http://localhost/sensmapserver/api/feeds/" + id, //USE /feeds/, because "/buildings/" would keep the plan tags
        headers: {
            'X-ApiKey': "17254faec6a60f58458308763"
        },
        body: data
        }, function(err, resp, body) {
            /// console.log(body);
    })
}

escapeRegExp = function (str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

parseFeedTags = function(tags) {
    var rtlstype = "";
    for (t in tags) {
        //#rtls_type#anchor, #rtls_type#tag
        if (tags[t].indexOf('#rtls_type') != -1) {
            var match = /#rtls_type#(.+)/.exec(tags[t]);
            if (match != null) {
                rtlstype = match[1];
            }
        }
    }
    return rtlstype;
}

removeTags = function (tags){
    var newtags = [];
    for (t in tags) {
        //#rtls_type#anchor, #rtls_type#tag
        if (tags[t].indexOf('#rtls_type') === -1) {
            newtags.push(tags[t]);
        }
    }
    return newtags;
}

findDatastreamById = function(datastreams, id) {
    if (!datastreams) { //empty array / undefined / null 
        return false;
    }
    for (var i = 0; i < datastreams.length; i++) {
        if (datastreams[i].id === id) {
            return datastreams[i];
        }
    }
    return false;
}

convertPlatform = function(tag) {//clear platform datastream for tags -> in order to convert Tag Li-ion to Tag IMU. Conversion itself is handled by RTLS Server
    var convertedDatastreams = [];

    if (tag.datastreams) {
        var hardware_version = findDatastreamById(tag.datastreams, "hardware_version");
        var platform = findDatastreamById(tag.datastreams, "platform");
        if (hardware_version && platform && hardware_version.current_value === "4.7" && platform.current_value === "TAG Li-ion") {
            return [{"id":"platform", "current_value":"TAG IMU"}];
        }
        if (platform && platform.current_value === "TAG Piccolino") {
            return [{"id":"platform", "current_value":"Piccolino"}];
        }
    } 
    return convertedDatastreams;
}

getFeeds(function(feeds) {
    if (feeds && feeds.results && feeds.results.length) {
        for (var i = 0; i < feeds.results.length; i++) { 
            if (feeds.results[i].id !== undefined && feeds.results[i].tags) {
            ////    console.log("------------- NEXT ITEM ----------- ")
                var type = parseFeedTags(feeds.results[i].tags);
             ////   console.log("OLD metaTAGS:",feeds.results[i].tags )
                if (type === "anchor" || type === "tag"){
                    var newTags = removeTags(feeds.results[i].tags);
              ////      console.log("Converting " + type + " - " + feeds.results[i].id );
                    updateFeed(feeds.results[i].id,{'tags':newTags, 'type':type});
                }
            ////    console.log("NEW metaTAGS:", newTags);
            } else {
                console.log("INFO: NO DEVICES DETECTED");
            }
            if (feeds.results[i].id !== undefined && feeds.results[i].datastreams) {
                ////console.log("converting tag's platform and hw:", findDatastreamById(feeds.results[i].datastreams, "platform"), findDatastreamById(feeds.results[i].datastreams, "hardware_version"))
                var convertedDatastreams = convertPlatform(feeds.results[i]);
                ////console.log("converted platform datastreams", convertedDatastreams);
                if (convertedDatastreams) {
                    updateFeed(feeds.results[i].id, {"datastreams" : convertedDatastreams});
                }

            }
        }
    } else {
        console.log("INFO: NO DEVICES DETECTED");
    }
})