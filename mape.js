/**
 * util functions for MAPE calculations
 */

var MAPE = {};

module.exports = MAPE;

/**
 * compute the error
 */
MAPE.error = function(val) {
  return ((val.actual - val.predicted) / val.actual);
}

/**
 * compute the squared error
 */
MAPE.squaredError = function(vals) {
  var errors = [];
  for (var i = vals.length - 1; i >= 0; i--) {
    errors.push(Math.abs(MAPE.error(vals[i])));
  };
  return errors;
}

/**
 * compute the mean
 */
MAPE.mean = function(vals) {
  var total = 0;
  for (var i = vals.length - 1; i >= 0; i--) {
    total += vals[i];
  }
  return total / vals.length;
}

/**
 * compute the mean square error
 */
MAPE.mse = function(vals) {
  return ((100 / vals.length) * (MAPE.mean(MAPE.squaredError(vals))));
}

MAPE.init = function(val_gen, val_gat) {
//   console.log("We generated "+ val_gen.length +" values and gathered "+val_gat.length+" values");
  var dataset = [];
  for(var i = 0; i < val_gat.length-1; i++) {
    var temp = {};
    temp.actual = val_gen[i];
    temp.predicted = val_gat[i];
    dataset.push(temp);
  }
  // console.log(dataset);
  return MAPE.mse(dataset)
}